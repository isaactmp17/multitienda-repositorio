<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->decimal('price',12,2);
            $table->decimal('old_price',12,2)->nullable();
            $table->decimal('wholesaler_price',12,2)->nullable();
            $table->text('stock');
            $table->string('slug')->nullable();
            $table->text('keywords')->nullable();
            $table->unsignedBigInteger('store_id')->nullable();
            $table->unsignedBigInteger('product_size_id')->nullable();
            $table->enum('type', ['s', 'a'])->default('s');
            $table->string('photo_1',5)->nullable();
            $table->string('photo_2',5)->nullable();
            $table->string('photo_3',5)->nullable();
            $table->boolean('is_featured')->default(false);
            $table->boolean('is_active')->default(true);
            $table->timestamps();

            $table->foreign('store_id')->references('id')->on('stores');
            $table->foreign('product_size_id')->references('id')->on('product_sizes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
