<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    public function run()
    {
        $roles=array('Super Administrador','Administrador','Cliente');
        foreach ($roles as $rol){
            $item=new Role();
            $item->name=$rol;
            $item->save();
        }
    }
}
