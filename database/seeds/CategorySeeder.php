<?php

use Illuminate\Database\Seeder;
use App\Category;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories=[
            'Jeans',
            'Blusas',
            'Ropa interior Y Vestidos de Baño',
            'Línea Infantil',
            'Pijamas',
            'Ropa Deportiva',
            'Camisetas',
            'Calzado',
            'Calzado Deportivo',
            'Oferta y Promociones',
            'Saldos Mayoristas',
            'Servicios Profesionales',
            'Labor Social'
        ];

        foreach ($categories as $category){
            $item = new Category();
            $item->name=$category;
            $item->slug=Str::slug($category, '-');
            $item->save();
        }
    }
}
