<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Isaac Petruccelli
        $item=new User();
        $item->fullname="Isaac Petruccelli";
        $item->email="isaactmp@gmail.com";
        $item->password=Hash::make('TfkaaHMtW)');
        $item->role_id=1;
        $item->save();

        // Mariano Pineda
        $item=new User();
        $item->fullname="Mariano Pineda";
        $item->email="web@ofimax.org";
        $item->password=Hash::make('Mr(H#x+G[N)');
        $item->role_id=1;
        $item->save();

        // Dueños
        $item=new User();
        $item->fullname="Administrador";
        $item->email="admin@madrugonmayorista.com";
        $item->password=Hash::make('7TSpenJw');
        $item->role_id=1;
        $item->save();
    }
}
