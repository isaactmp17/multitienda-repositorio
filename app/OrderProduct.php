<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $fillable = [
        'order_id', 'product_id', 'name','quanty','taxonomy','price'
    ];
    public function order()
    {
        return $this->belongsTo('App\Order','order_id');
    }
    public function product()
    {
        return $this->belongsTo('App\Product','product_id');
    }
}
