<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use App\Store;

class ManagerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()&&Auth::user()&&Auth::user()->role_id==2){
            if(!Store::where('user_id',Auth::user()->id)->exists()) {
                return redirect()->to('manager/no_store');
            }
            return $next($request);
        }
        return redirect(RouteServiceProvider::HOME);
    }
}
