<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Product;
use App\State;
use App\Store;
use Illuminate\Http\Request;
use Auth;

class PagesController extends Controller
{
    public function index()
    {
        $stores=Store::where('is_active',1)->get();
        $categories=Category::where('is_active',1)->get();
        $discount_products=Product::where('is_active',1)->whereNotNull('old_price')->inRandomOrder()->with('store')->paginate(4);
        $featured_products=Product::where('is_active',1)->where('is_featured',1)->inRandomOrder()->with('store')->paginate(4);
        if(Auth::check()){
            if(Auth::user()->role_id<3){
                return redirect()->to('/home');
            }
        }
        return view('welcome',compact('stores','categories','discount_products','featured_products'));
    }

    public function getRequest(Request $request,$action,$id)
    {
        if($request->ajax()){
            switch ($action){
                case 'cities':
                    if(State::where('id',$id)->exists()){
                        $items=City::where('state_id',$id)->orderBy('name','asc')->select('id','name')->get();
                        return $items;
                    }
                    abort(404);
                    break;
                default:
                    abort(404);
                    break;
            }
        }
        abort(404);

    }
}
