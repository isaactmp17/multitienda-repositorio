<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Product;
use App\ProductSize;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Image;
use DataTables;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->uri = 'products';
    }

    public function index(Request $request)
    {
        $uri=$this->uri;
        if ($request->ajax()) {
            $store = Store::where('user_id',Auth::user()->id)->first();
            $items = Product::orderBy('is_active')->where('store_id',$store->id)->get();
            return Datatables::of($items)
                ->addColumn('photo', function($item){
                    return '<img src="'.asset('statics/img/products/'.$item->id.'/photo_1_270x260.'.$item->photo_1.'?v='.strtotime($item->updated_at)).'" height="60px">';
                })
                ->addColumn('stock', function($item){
                    if($item->type=='s'){
                        return $item->stock;
                    }
                    $html='';
                    foreach (json_decode($item->stock) as $index=>$stock) {
                        if($index==0)
                            $html.='<strong>'.$stock[0].'</strong>: '.$stock[1];
                        else
                            $html.=' - <strong>'.$stock[0].'</strong>: '.$stock[1];
                    }
                    return $html;
                })
                ->addColumn('is_featured', function($item){
                    if($item->is_featured==1)
                        $html='<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input change-featured" id="switchf'.$item->id.'" data-target="'.$item->id.'" checked><label class="custom-control-label" for="switchf'.$item->id.'"></label></div>';
                    else
                        $html='<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input change-featured" id="switchf'.$item->id.'" data-target="'.$item->id.'"><label class="custom-control-label" for="switchf'.$item->id.'"></label></div>';
                    return $html;
                })
                ->addColumn('action', function($item){
                    $html1='<a href="/manager/products/'.$item->id.'/edit" class="edit btn btn-primary btn-sm py-1 px-3"><i class="fas fa-pencil-alt"></i></a>';
                    if($item->is_active==1)
                        $html2='<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input change-status" id="switch'.$item->id.'" data-target="'.$item->id.'" checked><label class="custom-control-label" for="switch'.$item->id.'"></label></div>';
                    else
                        $html2='<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input change-status" id="switch'.$item->id.'" data-target="'.$item->id.'"><label class="custom-control-label" for="switch'.$item->id.'"></label></div>';
                    return $html1.$html2;
                })
                ->rawColumns(['photo','stock','is_featured','action'])
                ->make(true);
        }
        return view("manager.$uri.index",compact('uri'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uri=$this->uri;
        $product_sizes=ProductSize::orderBy('name')->get();
        return view("manager.$uri.create_edit",compact('uri','product_sizes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uri=$this->uri;
        $request->validate([
            'photo_1' => 'required|mimes:jpeg,jpg,png|max:1024',
            'photo_2' => 'nullable|mimes:jpeg,jpg,png|max:1024',
            'photo_3' => 'nullable|mimes:jpeg,jpg,png|max:1024',
            'name' => 'required|max:120',
            'description' => 'required|max:190',
            'product_size_id' => 'required_if:type,a'
        ]);
        $user=Auth::user();
        $store=Store::where('user_id',$user->id)->first();
        $stock=0;
        if($request->type=='s'){
            $stock=is_null($request->stock)?0:$request->stock;
        }else{
            $product_size=ProductSize::findOrfail($request->product_size_id);
            $taxonomies=json_decode($product_size->value);
            if(count($taxonomies)==count($request->taxonomies)){
                $stock=array();
                foreach($taxonomies as $index=>$taxonomy){
                    array_push($stock,[$taxonomy,$request->taxonomies[$index]]);
                }
                $stock=json_encode($stock);
            }else{
                return redirect()->to("manager/$uri")->with(['error'=>'Hubo un problema al intentar crear su producto, intente nuevamente.']);
            }
        }
        $item=new Product();
        $item->name=$request->name;
        $item->description=$request->description;
        $item->price=$request->price;
        $item->wholesaler_price=$request->wholesaler_price;
        $item->old_price=$request->old_price;
        $item->stock=$stock;
        $item->store_id=$store->id;
        $item->product_size_id=$request->product_size_id;
        $item->type=$request->type;
        $item->save();

        for($i=1;$i<=3;$i++){
            $string='photo_'.$i;
            if($request->hasFile($string)){
                $file = $request->file($string);
                $this->uploadProductImageFile($string,$item->id,$file);
                $item->$string=$file->getClientOriginalExtension();
            }
        }
        $item->slug=$this->createSlug($request->name,$item->id);
        $item->save();
        return redirect()->to("manager/$uri")->with(['success'=>'El producto ha sido creado correctamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $uri=$this->uri;
        $item=Product::findOrFail($id);
        $product_sizes=ProductSize::orderBy('name')->get();
        return view("manager.$uri.create_edit",compact('uri','item','product_sizes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $uri=$this->uri;
        $request->validate([
            'photo_1' => 'nullable|mimes:jpeg,jpg,png|max:1024',
            'photo_2' => 'nullable|mimes:jpeg,jpg,png|max:1024',
            'photo_3' => 'nullable|mimes:jpeg,jpg,png|max:1024',
            'name' => 'required|max:120',
            'description' => 'required|max:190'
        ]);


        $item=Product::findOrFail($id);
        $stock=0;
        if($item->type=='s'){
            $stock=is_null($request->stock)?0:$request->stock;
        }else{
            $stock=array();
            $taxonomies=json_decode($item->stock);
            foreach($taxonomies as $index=>$taxonomy){
                array_push($stock,[$taxonomy[0],$request->taxonomies[$index]]);
            }
            $stock=json_encode($stock);
        }
        $item->name=$request->name;
        $item->description=$request->description;
        $item->price=$request->price;
        $item->wholesaler_price=$request->wholesaler_price;
        $item->old_price=$request->old_price;
        $item->stock=$stock;
        $item->slug=$this->createSlug($request->name,$item->id);
        for($i=1;$i<=3;$i++){
            $string='photo_'.$i;
            if($request->hasFile($string)){
                $file = $request->file($string);
                $this->uploadProductImageFile($string,$item->id,$file);
                $item->$string=$file->getClientOriginalExtension();
            }
        }
        $item->save();
        return redirect()->to("manager/$uri")->with(['success'=>'El producto ha sido actualizado correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getProductSizes(Request $request)
    {
        $id=intval($request->id);
        if(ProductSize::where('id',$id)->exists()){
            $item=ProductSize::where('id',$id)->first();
            return view('manager.products.show_taxonomy', compact('item'))->render();
        }

        return $request->all();
    }

    public function uploadProductImageFile($string,$item_id,$file)
    {

        $ImageUploadFull = Image::make($file);
        $ImageUploadFeatured = Image::make($file)->fit(270, 370);
        $ImageUploadNormal = Image::make($file)->fit(270, 260);
        $originalPath = 'statics/img/products/'.$item_id.'/';
        if (!file_exists($originalPath)) {
            mkdir($originalPath, 755, true);
        }
        $ImageUploadFull->save($originalPath.$string.'_full.'.$file->getClientOriginalExtension());
        $ImageUploadNormal->save($originalPath.$string.'_270x260.'.$file->getClientOriginalExtension());
        if($string=='photo_1')
            $ImageUploadFeatured->save($originalPath.$string.'_270x370.'.$file->getClientOriginalExtension());

    }


    public function updateStatus(Request $request)
    {
        if($request->ajax()){
            if(isset($request->id)){
                $id=intval($request->id);
                $item=Product::findOrFail($id);
                if($item->is_active==1)
                    $item->is_active=0;
                else
                    $item->is_active=1;
                $item->save();
                return ['band'=>'1'];
            }
            return ['band'=>'0'];
        }
        abort(404);
    }

    public function updateFeatured(Request $request)
    {
        if($request->ajax()){
            if(isset($request->id)){
                $id=intval($request->id);
                $user=Auth::user();
                $item=Product::findOrFail($id);
                $store=Store::where('user_id',$user->id)->first();
                if(Product::where('store_id',$store->id)->where('is_featured',1)->count()<4){
                    if($item->is_featured==1)
                        $item->is_featured=0;
                    else
                        $item->is_featured=1;
                    $item->save();
                    return ['band'=>'1'];
                }
                return ['band'=>'2'];

            }
            return ['band'=>'0'];
        }
        abort(404);
    }

    public function createSlug($name,$id)
    {
        $slug=Str::slug($name, '-');

        return $id.'-'.$slug;
    }
}
