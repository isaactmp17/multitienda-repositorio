<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Order;
use App\Store;
use Illuminate\Http\Request;
use Purify;
use Auth;


class OrderController extends Controller
{
    //

    public function index($code)
    {
        $code=trim(Purify::clean($code));
        $store=Store::where('user_id',Auth::user()->id)->first();
        if(Order::where('order_reference',$code)->where('store_id',$store->id)->exists()){
            $order=Order::where('order_reference',$code)->where('store_id',$store->id)->with(['store','orderProducts','city','city.state'])->first();
            return view('manager.view_order',compact('item','order'));
        }
        abort(404);
    }

    public function updateStatus(Request $request,$code)
    {
        $code=trim(Purify::clean($code));
        $store=Store::where('user_id',Auth::user()->id)->first();
        if(Order::where('order_reference',$code)->where('store_id',$store->id)->exists()){
            $validatedData = $request->validate([
                'status' => ['required', 'numeric','min:0', 'max:4'],
            ]);
            $order=Order::where('order_reference',$code)->where('store_id',$store->id)->first();
            $order->status=$request->status;
            $order->save();
            return redirect()->to('/home')->with('success',"Se ha actualizado el pedido $code correctamente.");

        }
        abort(404);
    }
}
