<?php

namespace App\Http\Controllers\Administrador;

use App\Http\Controllers\Controller;
use App\ProductSize;
use Illuminate\Http\Request;
use DataTables;

class ProductSizesController extends Controller
{
    public function __construct()
    {
        $this->uri = 'product_sizes';
    }

    public function index(Request $request)
    {
        $uri=$this->uri;
        if ($request->ajax()) {
            $items = ProductSize::orderBy('name')->get();
            return Datatables::of($items)
                ->addColumn('action', function($item) use($uri){
                    $html1='<a href="/admin/'.$uri.'/'.$item->id.'/edit" class="edit btn btn-primary btn-sm py-1 px-3"><i class="fas fa-pencil-alt"></i></a>';
                    return $html1;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view("administrador.$uri.index",compact('uri'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uri=$this->uri;
        return view("administrador.$uri.create_edit",compact('uri'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uri=$this->uri;
        $messages=[
            'options.*'=>'Los atributos son obligatorios'
        ];
        $request->validate([
            'name' => 'required|max:120',
            'options' => 'required|array|min:1',
            "options.*"  => "required|string|max:120",
        ],$messages);

        $item=new ProductSize();
        $item->name=$request->name;
        $item->value=json_encode($request->options);
        $item->save();
        return redirect()->to("admin/$uri")->with(['success'=>'La taxonomía ha sido creada correctamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $uri=$this->uri;
        $item=ProductSize::findOrFail($id);
        return view("administrador.$uri.create_edit",compact('uri','item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $uri=$this->uri;
        $messages=[
            'options.*'=>'Los atributos son obligatorios'
        ];
        $request->validate([
            'name' => 'required|max:120',
            'options' => 'required|array|min:1',
            "options.*"  => "required|string|max:120",
        ],$messages);

        $item=ProductSize::findOrfail($id);
        $item->name=$request->name;
        $item->value=json_encode($request->options);
        $item->save();
        return redirect()->to("admin/$uri")->with(['success'=>'La taxonomía ha sido actualizada correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
