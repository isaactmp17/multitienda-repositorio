<?php

namespace App\Http\Controllers\Administrador;

use App\Category;
use App\Store;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Str;
use DataTables;
use Image;

class StoreController extends Controller
{

    public function __construct()
    {
        $this->uri = 'stores';
    }
    public function index(Request $request)
    {
        $uri=$this->uri;
        if ($request->ajax()) {
            $items = Store::with('user')->get();
            return Datatables::of($items)
                ->addColumn('photo', function($item){
                    return '<img src="'.asset('statics/img/stores/'.$item->id.'.'.$item->photo.'?v='.time()).'" height="40px">';
                })
                ->addColumn('owner', function($item){
                    if($item->user)
                        return $item->user->fullname;
                    return 'No asignado';
                })
                ->addColumn('action', function($item){
                    $html1='<a href="/admin/stores/'.$item->id.'/edit" class="edit btn btn-primary btn-sm py-1 px-3"><i class="fas fa-pencil-alt"></i></a>';
                    if($item->is_active==1)
                        $html2='<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input change-status" id="switch'.$item->id.'" data-target="'.$item->id.'" checked><label class="custom-control-label" for="switch'.$item->id.'"></label></div>';
                    else
                        $html2='<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input change-status" id="switch'.$item->id.'" data-target="'.$item->id.'"><label class="custom-control-label" for="switch'.$item->id.'"></label></div>';
                    return $html1.$html2;
                })
                ->rawColumns(['photo','action'])
                ->make(true);
        }
        return view("administrador.$uri.index",compact('uri'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uri=$this->uri;
        $users=User::whereIn('role_id',[2])->doesntHave('store')->orderBy('fullname')->get();
        return view("administrador.$uri.create_edit",compact('uri','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uri=$this->uri;
        $request->validate([
            'logo' => 'required|mimes:jpeg,jpg,png|max:1024',
            'name' => 'required|max:120',
            'nit' => 'max:120',
            'description' => 'max:190',
        ]);
        $item = new Store($request->all());
        $item->slug=$this->createSlug($request->name);;
        $item->save();
        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $ImageUpload = Image::make($file)->resize(300, 180, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $originalPath = 'statics/img/stores/';
            if (!file_exists($originalPath)) {
                mkdir($originalPath, 755, true);
            }
            $ImageUpload->save($originalPath.$item->id.'.'.$file->getClientOriginalExtension());
            $item->photo=$file->getClientOriginalExtension();
            $item->save();
        }
        return redirect()->to("admin/$uri")->with(['success'=>'La tienda ha sido creada correctamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort('404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $uri=$this->uri;
        $item=Store::findOrFail($id);
        $users=User::whereHas('store',function($q) use($item){
            $q->where('user_id',$item->user_id);
        })->orDoesntHave('store')->whereIn('role_id',[2])->orderBy('fullname')->get();
        return view("administrador.$uri.create_edit",compact('uri','item','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $uri=$this->uri;
        $request->validate([
            'logo' => 'mimes:jpeg,jpg,png|max:1024',
            'name' => 'required|max:120',
            'nit' => 'max:120',
            'description' => 'max:190',
        ]);
        $item = Store::findOrFail($id);
        $inputs = $request->all();
        if($item->name!=$request->name)
            $item->slug=$this->createSlug($request->name);
        $item->fill($inputs)->save();
        $item->touch();
        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $ImageUpload = Image::make($file)->resize(150, 90, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $originalPath = 'statics/img/stores/';
            if (!file_exists(public_path($originalPath))) {
                mkdir(public_path($originalPath), 755, true);
            }
            $ImageUpload->save($originalPath.$item->id.'.'.$file->getClientOriginalExtension());
            $item->photo=$file->getClientOriginalExtension();
            $item->save();
        }
        return redirect()->to("admin/$uri")->with(['success'=>'La tienda ha sido actualizada correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort('404');
    }

    public function updateStatus(Request $request)
    {
        if($request->ajax()){
            if(isset($request->id)){
                $id=intval($request->id);
                $item=Store::findOrFail($id);
                if($item->is_active==1)
                    $item->is_active=0;
                else
                    $item->is_active=1;
                $item->save();
                return ['band'=>'1'];
            }
            return ['band'=>'0'];
        }
        abort(404);
    }

    public function indexStoreCategories()
    {
        $items=Store::orderBy('name')->paginate(10);
        return view('administrador.stores.index_store_categories',compact('items'));
    }
    public function storeCategories($id)
    {
        $item=Store::with('categories')->findOrFail($id);
        $categories=Category::where('is_active',1)->get();
        $categories_selected=array();
        foreach($item->categories as $category_selected){
            array_push($categories_selected,$category_selected->id);
        }
        return view('administrador.stores.edit_store_categories',compact('item','categories','categories_selected'));
    }

    public function updateStoreCategories(Request $request,$id)
    {
        $uri=$this->uri;
        $id=intval($id);
        $item=Store::findOrFail($id);
        $item->categories()->detach();
        $item->categories()->attach($request->categories);
        return redirect()->to("admin/$uri/categories")->with(['success'=>'La tienda ha sido actualizada correctamente.']);
    }

    public function createSlug($name)
    {
        $slug=Str::slug($name, '-');
        $band=0;
        $control=0;
        do{
            if($control==0){
                $slug_full=$slug;
            }else{
                $slug_full=$slug.'-'.$control;
            }
            if(!Store::where('slug',$slug_full)->exists()){
                $band=1;
            }else{
                $control++;
            }

        }while($band==0);

        return $slug_full;
    }
}
