<?php

namespace App\Http\Controllers\Administrador;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->uri = 'categories';
    }

    public function index(Request $request)
    {
        $uri=$this->uri;
        if ($request->ajax()) {
            $items = Category::orderBy('is_active')->get();
            return Datatables::of($items)
                ->addColumn('action', function($item) use($uri){
                    $html1='<a href="/admin/'.$uri.'/'.$item->id.'/edit" class="edit btn btn-primary btn-sm py-1 px-3"><i class="fas fa-pencil-alt"></i></a>';
                    if($item->is_active==1)
                        $html2='<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input change-status" id="switch'.$item->id.'" data-target="'.$item->id.'" checked><label class="custom-control-label" for="switch'.$item->id.'"></label></div>';
                    else
                        $html2='<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input change-status" id="switch'.$item->id.'" data-target="'.$item->id.'"><label class="custom-control-label" for="switch'.$item->id.'"></label></div>';
                    return $html1.$html2;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view("administrador.$uri.index",compact('uri'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uri=$this->uri;
        return view("administrador.$uri.create_edit",compact('uri'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uri=$this->uri;
        $request->validate([
            'name' => 'required|max:120',
            'description' => 'max:190',
        ]);
        $item = new Category($request->all());
        $item->slug=$this->createSlug($request->name);
        $item->save();
        return redirect()->to("admin/$uri")->with(['success'=>'La categoría ha sido creada correctamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $uri=$this->uri;
        $item=Category::findOrFail($id);
        return view("administrador.$uri.create_edit",compact('uri','item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $uri=$this->uri;
        $request->validate([
            'logo' => 'mimes:jpeg,jpg,png|max:1024',
            'name' => 'required|max:120',
            'nit' => 'max:120',
            'description' => 'max:190',
        ]);
        $item = Category::findOrFail($id);
        $inputs = $request->all();
        if($item->name!=$request->name)
            $item->slug=$this->createSlug($request->name);
        $item->fill($inputs)->save();
        return redirect()->to("admin/$uri")->with(['success'=>'La categoría ha sido actualizada correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }
    public function updateStatus(Request $request)
    {
        if($request->ajax()){
            if(isset($request->id)){
                $id=intval($request->id);
                $item=Category::findOrFail($id);
                if($item->is_active==1)
                    $item->is_active=0;
                else
                    $item->is_active=1;
                $item->save();
                return ['band'=>'1'];
            }
            return ['band'=>'0'];
        }
        abort(404);
    }
    public function createSlug($name)
    {
        $slug=Str::slug($name, '-');
        $band=0;
        $control=0;
        do{
            if($control==0){
                $slug_full=$slug;
            }else{
                $slug_full=$slug.'-'.$control;
            }
            if(!Category::where('slug',$slug_full)->exists()){
                $band=1;
            }else{
                $control++;
            }

        }while($band==0);

        return $slug_full;
    }
}
