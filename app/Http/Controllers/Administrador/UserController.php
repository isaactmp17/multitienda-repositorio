<?php

namespace App\Http\Controllers\Administrador;

use App\Role;
use App\State;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Purify;

class UserController extends Controller
{
    public function __construct()
    {
        $this->uri = 'users';
    }

    public function index(Request $request)
    {
        $uri=$this->uri;
        if ($request->ajax()) {
            $items = User::select(['id','fullname','email','role_id','city_id','is_active'])->with('role','city')->where('id','>',1)->where('id','<>',Auth::user()->id)->get();
            return Datatables::of($items)
                ->addColumn('role', function($item){
                    return $item->role->name;
                })
                ->addColumn('city', function($item){
                    if($item->city)
                        return $item->city->name;
                    return 'No asignado';
                })
                ->addColumn('action', function($item){
                    if($item->city)
                        return $item->city->name;
                    return 'No asignado';
                })
                ->addColumn('action', function($item){
                    $html1='<a href="/admin/users/'.$item->id.'/edit" class="edit btn btn-primary btn-sm py-1 px-3"><i class="fas fa-pencil-alt"></i></a>';
                    if($item->is_active==1)
                        $html2='<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input change-status" id="switch'.$item->id.'" data-target="'.$item->id.'" checked><label class="custom-control-label" for="switch'.$item->id.'"></label></div>';
                    else
                        $html2='<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input change-status" id="switch'.$item->id.'" data-target="'.$item->id.'"><label class="custom-control-label" for="switch'.$item->id.'"></label></div>';
                    return $html1.$html2;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $users=User::select('id','role_id','is_active')->get();
        return view("administrador.$uri.index",compact('uri','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uri=$this->uri;
        $states=State::where('status',1)->orderBy('name')->get();
        $roles=Role::all();
        return view("administrador.$uri.create_edit",compact('uri','states','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uri=$this->uri;
        $request->validate([
            'fullname' => 'string|required|max:120',
            'email' => 'email|unique:users',
            'password' => 'string|min:6|max:120',
            'document_type' => 'nullable',
            'document_number' => 'required_with:document_type|max:120',
            'state_id' => 'nullable',
            'city_id' => 'required_with:state_id',
            'address' => 'nullable',
        ]);
        $item = new User($request->all());
        $item->password=Hash::make($request->password);
        if(!is_null($request->address))
            $item->address=Purify::clean($request->address);
        $item->save();
        return redirect()->to("admin/$uri")->with(['success'=>'El usuario ha sido creado correctamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $uri=$this->uri;
        $item=User::with('city.state')->findOrFail($id);
        $states=State::where('status',1)->orderBy('name')->get();
        $roles=Role::all();
        return view("administrador.$uri.create_edit",compact('uri','item','states','cities','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $uri=$this->uri;
        $request->validate([
            'fullname' => 'string|required|max:120',
            'password' => 'string|min:6|max:120',
            'document_type' => 'nullable',
            'document_number' => 'required_with:document_type|max:120',
            'state_id' => 'nullable',
            'city_id' => 'required_with:state_id',
            'address' => 'nullable',
        ]);
        $item = User::findOrFail($id);
        $inputs = $request->all();
        $item->fill($inputs)->save();
        return redirect()->to("admin/$uri")->with(['success'=>'El usuario ha sido actualizado correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort('404');
    }

    public function updateStatus(Request $request)
    {
        if($request->ajax()){
            if(isset($request->id)){
                $id=intval($request->id);
                $item=User::findOrFail($id);
                if($item->is_active==1)
                    $item->is_active=0;
                else
                    $item->is_active=1;
                $item->save();
                return ['band'=>'1'];
            }
            return ['band'=>'0'];
        }
        abort(404);
    }
}
