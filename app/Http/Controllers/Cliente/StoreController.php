<?php

namespace App\Http\Controllers\Cliente;

use App\Category;
use App\Helper;
use App\Http\Controllers\Controller;
use App\Product;
use App\Store;
use Illuminate\Http\Request;
use PHPUnit\TextUI\Help;
use Purify;

class StoreController extends Controller
{
    protected $helper;
    public function __construct()
    {
        $this->helper=new Helper();
    }
    public function index($slug)
    {
        $slug = Purify::clean($slug);
        if (Store::where('slug', $slug)->exists()){
            $item = Store::where('slug', $slug)->first();
            $shop_cart=$this->helper->getCart($item->id);
            $categories=Category::where('is_active',1)->get();
            $products_featured=Product::where('store_id',$item->id)->where('is_featured',1)->where('is_active',1)->get();
            $products=Product::where('store_id',$item->id)->where('is_featured',0)->where('is_active',1)->paginate(8);
            return view('cliente.index_store',compact('item','categories','products_featured','products','shop_cart'));
        }
        abort(404);
    }
    public function indexCart($slug)
    {
        $slug = Purify::clean($slug);
        if (Store::where('slug', $slug)->exists()){
            $item = Store::where('slug', $slug)->first();
            $shop_cart_items=$this->helper->getCart($item->id);
            $categories=Category::where('is_active',1)->get();
            return view('cliente.index_cart',compact('item','categories','products_featured','products','shop_cart','shop_cart_items'));
        }
        abort(404);
    }

    public function viewProduct($slug,$slug_product)
    {
        $slug = Purify::clean($slug);
        $slug_product = Purify::clean($slug_product);
        if (Store::where('slug', $slug)->whereHas('products',function ($q) use($slug_product){
            $q->where('slug',$slug_product);
        })->exists()){
            $store = Store::where('slug', $slug)->first();
            $shop_cart=$this->helper->getCart($store->id);
            $categories=Category::where('is_active',1)->get();
            $item = Product::where('slug', $slug_product)->with('productSize')->first();
            return view('cliente.view_product',compact('store','item','categories','shop_cart'));
        }
        abort(404);

    }

    public function viewCategory($slug)
    {
        $slug = Purify::clean($slug);
        if (Category::where('slug', $slug)->exists()){
            $item = Category::where('slug', $slug)->first();
            $stores=$item->stores()->orderBy('name')->paginate(10);
            $categories=Category::where('is_active',1)->get();
            return view('cliente.index_category',compact('item','categories','stores'));
        }
        abort(404);
    }


    public function serchProducts(Request $request)
    {
        if($request->has('q')){
            $query=Purify::clean($request->q);
            $categories=Category::where('is_active',1)->get();
            $items=Product::where("name","like","%$query%")->orWhere("keywords","like","%$query%")->with('store')->paginate(8);
            return view('cliente.search_results',compact('categories','query','items'));
        }
        abort(404);
    }



    public function addToCart(Request $request,$slug_store,$slug_product)
    {
        $slug_store = Purify::clean($slug_store);
        $slug_product = Purify::clean($slug_product);
        $request->validate([
            'store_id' => 'required|integer',
            'product_id' => 'required|integer',
        ]);
        $store_id = intval(Purify::clean($request->store_id));
        $product_id = intval(Purify::clean($request->product_id));
        if (Store::where('id', $store_id)->where('slug', $slug_store)->whereHas('products', function ($q) use ($slug_product, $product_id) {
            $q->where('id', $product_id)->where('slug', $slug_product);
        })->exists()) {
            $attributeNames = array(
                'quanty' => 'de cantidad',
            );
            $request->validate([
                'quanty' => 'required|integer|min:1',
            ], [], $attributeNames);
            $item = Product::findOrFail($product_id);
            $product_taxonomy = null;
            if ($request->has('product_taxonomy'))
                $product_taxonomy = $request->product_taxonomy;
            if ($this->helper->checkStock($item, $request->quanty, $product_taxonomy)) {
                $response = $this->helper->addProductToCart($store_id, $product_id, $request->quanty, $product_taxonomy);
                if ($response['band'] == 1) {
                    return redirect()->to('/tienda/' . $slug_store)->with('success', $response['msg']);
                }
                return redirect()->back()->with('error', $response['msg'])->withInput();
            }
            $stock_item = 0;
            if ($item->type == 's') {
                $stock_item = $item->stock;
            } else {
                foreach (json_decode($item->stock) as $taxonomy) {
                    if ($taxonomy[0] == $product_taxonomy) {
                        $stock_item = $taxonomy[1];
                        break;
                    }
                }
            }
            return redirect()->back()->with('error_stock', ['msg' => 'Las unidades disponibles han cambiado a: ' . $stock_item, 'stock' => $stock_item])->withInput();
        }
        abort(404);

    }
}
