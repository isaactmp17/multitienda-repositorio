<?php

namespace App\Http\Controllers\Cliente;

use App\Category;
use App\Helper;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\State;
use App\Store;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Purify;
use Illuminate\Http\Request;

class CheckOutController extends Controller
{


    protected $helper;
    public function __construct()
    {
        $this->helper=new Helper();
    }

    public function login(Request $request,$slug)
    {
        $slug = Purify::clean($slug);
        $item = Store::where('slug', $slug)->first();
        $email = null;
        $passed = false;
        $states = State::where('status',1)->orderBy('name')->get();
        if ($item) {
            $shop_cart_items = $this->helper->getCart($item->id);

            $categories = Category::where('is_active', 1)->get();
            if (count($shop_cart_items->items) > 0) {
                if (!Auth::check()) {
                    if ($request->has('email')) {
                        $validatedData = $request->validate([
                            'email' => 'required|email:rfc,dns',
                        ]);
                        $email = trim(Purify::clean($request->email));
                        if (!$request->has('password')) {
                            if (User::where('email', $email)->exists()) {
                                $passed = true;
                            }
                        }
                    }
                } else {
                    if (Auth::user()->role_id != 3) {
                        return redirect()->to('login');
                    }
                }
                return view('cliente.login_cart', compact('item', 'categories', 'products_featured', 'products', 'shop_cart', 'shop_cart_items', 'email', 'passed','states'));
            }
            return redirect()->to('/tienda/' . $item->slug);
        }
        abort(404);
    }

    public function loginPost(Request $request,$slug)
    {
        $slug = Purify::clean($slug);
        $item = Store::where('slug', $slug)->first();
        if ($item) {
            $validatedData = $request->validate([
                'email' => 'required|email:rfc,dns',
                'password' => 'required|string|max:90',
            ]);
            $email = trim(Purify::clean($request->email));
            if (Auth::attempt(['email' => $email, 'password' => $request->password, 'is_active' => 1])) {
                return redirect()->to('tienda/' . $item->slug . '/cart/login');
            }
            return redirect()->back()->with(['error'=>'Por favor verifica las credenciales.']);
        }
        abort(404);
    }

    public function register(Request $request,$slug)
    {
        $slug = Purify::clean($slug);
        $item = Store::where('slug', $slug)->first();
        if ($item) {
            $validatedData = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:190', 'unique:users'],
                'document_type' => ['required', 'string', 'max:190'],
                'state_id' => ['required', 'numeric'],
                'city_id' => ['required', 'numeric'],
                'document_number' => ['required', 'string', 'max:190'],
                'address' => ['required', 'string', 'max:190'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ],[
                'state_id.required'=>'El departamento es obligatorio',
                'city_id.required'=>'La ciudad es obligatoria'
            ]);
            $user = User::create([
                'fullname' => $request->name,
                'email' => $request->email,
                'document_type' => $request->document_type,
                'document_number' => $request->document_number,
                'address' => $request->address,
                'city_id' => $request->city_id,
                'password' => Hash::make($request->password),
            ]);
            Auth::login($user);
            return redirect()->to('tienda/' . $item->slug . '/cart/login');
        }
        abort(404);
    }
    public function deleteItem(Request $request,$slug)
    {
        $slug=Purify::clean($slug);
        $store=Store::where('slug',$slug)->first();
        $taxonomy=null;
        if($store){
            if($request->has('target')&&$request->has('taxonomy')){
                if($request->taxonomy)
                    $taxonomy=$request->taxonomy;
                $id=intval($request->target);
                $shop_cart=$this->helper->getCart($store->id);
                $cart_items=array();
                foreach($shop_cart->items as $item){
                    if(is_null($taxonomy)) {
                        if($item['id']!=$id) {
                            array_push($cart_items, $item);
                        }
                    }else{
                        if($item['id']==$id){
                            if ($item['taxonomy'] != $taxonomy) {
                                array_push($cart_items, $item);
                            }
                        }else{
                            array_push($cart_items, $item);
                        }

                    }
                }
                session(['cart'.$store->id=>$cart_items]);
                return ['band'=>1];
            }
        }
        abort(404);
    }
    private function generateRandomString($length) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    private function generateCode(){
        $band=0;
        do{
            $code=$this->generateRandomString(10);
            if(!Order::where('order_reference',$code)->exists())
                $band=1;
        }while($band==0);
        return $code;
    }
    public function checkOut(Request $request,$slug)
    {
        $slug=Purify::clean($slug);
        $item = Store::where('slug', $slug)->first();
        if($item){
            $shop_cart_items = $this->helper->getCart($item->id);
            if (count($shop_cart_items->items) > 0) {
                $validatedData = $request->validate([
                    'name' => ['required', 'string', 'max:255'],
                    'state_id' => ['required', 'numeric'],
                    'city_id' => ['required', 'numeric'],
                    'address' => ['required', 'string', 'max:190'],
                    'phone' => ['required','regex:/[0-9]{7}/'],
                ],[
                    'state_id.required'=>'El departamento es obligatorio',
                    'city_id.required'=>'La ciudad es obligatoria'
                ]);
                $total_ammount=0;
                foreach($shop_cart_items->items as $itemc) {
                    $price=0;
                    if($itemc['available_stock']>0){
                        if($itemc['quanty']>$itemc['available_stock']){
                            return redirect()->to('/tienda/'.$item->slug.'/cart');
                        }else{
                            if($itemc['quanty']>5){
                                $price=$itemc['details']->wholesaler_price;
                            }else{
                                $price=$itemc['details']->price;
                            }
                            $total_ammount+=$itemc['quanty']*$price;
                        }
                    }else{
                        return redirect()->to('/tienda/'.$item->slug.'/cart');
                    }
                }
                $price=0;
                //dd($shop_cart_items->items);
                $order=new Order();
                $order->order_reference=$this->generateCode();
                $order->store_id=$item->id;
                $order->user_id=Auth::user()->id;
                $order->city_id=$request->city_id;
                $order->fullname=$request->name;
                $order->address=$request->address;
                $order->phone=$request->phone;
                $order->total_ammount=$total_ammount;
                $order->save();

                foreach($shop_cart_items->items as $itemc) {
                    if($itemc['quanty']>5){
                        $price=$itemc['details']->wholesaler_price;
                    }else{
                        $price=$itemc['details']->price;
                    }

                    $product=Product::where('id',$itemc['id'])->first();
                    if($product->type=='s'){
                        $product->stock=$product->stock-$itemc['quanty'];
                    }else{
                        $newstock=array();
                        foreach (json_decode($product->stock) as $taxo){
                            if($taxo[0]==$itemc['taxonomy']){
                                array_push($newstock,[$taxo[0],$taxo[1]-$itemc['quanty']]);
                            }else{
                                array_push($newstock,[$taxo[0],$taxo[1]]);
                            }
                        }
                        $product->stock=json_encode($newstock);
                    }
                    $product->save();

                    $orderproduct = new OrderProduct();
                    $orderproduct->product_id = $itemc['id'];
                    $orderproduct->name = $itemc['details']->name;
                    $orderproduct->quanty = $itemc['quanty'];
                    $orderproduct->taxonomy = $itemc['taxonomy'];
                    $orderproduct->price = $price;
                    $order->orderProducts()->save($orderproduct);

                }
                session()->forget('cart'.$item->id);
                return redirect()->to("tienda/$item->slug/order/$order->order_reference");
            }
        }
    }

    public function viewOrder($slug,$code)
    {
        $slug=trim(Purify::clean($slug));
        $code=trim(Purify::clean($code));
        $item = Store::where('slug', $slug)->first();
        if($item){
            $order = Order::where('order_reference', $code)->where('user_id',Auth::user()->id)->where('store_id',$item->id)->with(['store','orderProducts','city','city.state'])->first();
            if($order){
                return view('cliente.view_order',compact('item','order'));
            }
        }
        abort(404);

    }
    public function postPayu(Request $request)
    {
        $ApiKey = "J3glV63gOKljXFOPd14YEw0hdi";
        $merchant_id = $request->merchantId;
        $referenceCode = $request->referenceCode;
        $TX_VALUE = $request->TX_VALUE;
        $New_value = number_format($TX_VALUE, 1, '.', '');
        $currency = $request->currency;
        $transactionState = $request->transactionState;
        $firma_cadena = "$ApiKey~$merchant_id~$referenceCode~$New_value~$currency~$transactionState";
        $firmacreada = md5($firma_cadena);
        $firma = $request->signature;
        $reference_pol = $request->reference_pol;
        $cus = $request->cus;
        $extra1 = $request->description;
        $transactionId = $request->transactionId;
        $newstatus=0;

        if ($request->transactionState == 4 ) {
            $estadoTx = "Transacción aprobada";
            $newstatus=2;
        }else if ($request->transactionState == 6 ) {
            $estadoTx = "Transacción rechazada";
            $newstatus=0;
        }else if ($request->transactionState == 104 ) {
            $estadoTx = "Error";
            $newstatus=1;
        }else if ($request->transactionState == 7 ) {
            $estadoTx = "Transacción pendiente";
            $newstatus=1;
        }else {
            $newstatus=1;
            $estadoTx=$request->mensaje;
        }


        if (strtoupper($firma) == strtoupper($firmacreada)) {
            if(Order::where('order_reference',$referenceCode)->exists()){
                $order=Order::where('order_reference',$referenceCode)->first();
                $order->status=$newstatus;
                $order->reference_pol=Purify::clean($reference_pol);
                $order->payu_response=Purify::clean($estadoTx);
                $order->save();

                return response()->json([],200);
            }
        }
        abort(404);
    }

    public function getPayu(Request $request)
    {
        $ApiKey = "J3glV63gOKljXFOPd14YEw0hdi";
        $merchant_id = $request->merchantId;
        $referenceCode = $request->referenceCode;
        $TX_VALUE = $request->TX_VALUE;
        $New_value = number_format($TX_VALUE, 1, '.', '');
        $currency = $request->currency;
        $transactionState = $request->transactionState;
        $firma_cadena = "$ApiKey~$merchant_id~$referenceCode~$New_value~$currency~$transactionState";
        $firmacreada = md5($firma_cadena);
        $firma = $request->signature;
        $reference_pol = $request->reference_pol;
        $cus = $request->cus;
        $extra1 = $request->description;
        $transactionId = $request->transactionId;
        $newstatus=0;

        if ($request->transactionState == 4 ) {
            $estadoTx = "Transacción aprobada";
            $newstatus=2;
        }else if ($request->transactionState == 6 ) {
            $estadoTx = "Transacción rechazada";
            $newstatus=0;
        }else if ($request->transactionState == 104 ) {
            $estadoTx = "Error";
            $newstatus=1;
        }else if ($request->transactionState == 7 ) {
            $estadoTx = "Transacción pendiente";
            $newstatus=1;
        }else {
            $newstatus=1;
            $estadoTx=$request->mensaje;
        }

        if (strtoupper($firma) == strtoupper($firmacreada)) {
            return redirect()->to('/home');
        }
        abort(404);
    }
}
