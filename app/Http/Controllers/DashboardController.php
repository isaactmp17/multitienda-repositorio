<?php

namespace App\Http\Controllers;

use App\OrderProduct;
use App\Product;
use App\Store;
use Illuminate\Http\Request;
use Auth;
use App\Order;
use DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['check_status','auth']);
    }

    public function index()
    {
        if(Auth::user()->role_id==1){
            $orders=Order::where('created_at', '>=', now()->subDays(10)->toDateString())
                ->get();
            $top_products=OrderProduct::with('product')->select( 'product_id',DB::raw('COUNT(*) as cnt'))->groupBy('product_id')->orderBy('cnt', 'DESC')->take(5)->get();
            return view('administrador.dashboard',compact('orders','top_products'));
        }
        if(Auth::user()->role_id==2){
            if(Store::where('user_id',Auth::user()->id)->exists()){
                $store=Store::where('user_id',Auth::user()->id)->first();
                $orders=Order::where('store_id',$store->id)->orderByRaw('FIELD(status, "3", "2", "1", "4", "0")')->with('store')->paginate(10);
                return view('manager.dashboard',compact('orders','store'));
            }

            return redirect()->to('manager/no_store');
        }
        if(Auth::user()->role_id==3){
            $orders=Order::where('user_id',Auth::user()->id)->orderByRaw('FIELD(status, "3", "2", "1", "4", "0")')->with('store')->paginate(10);
            return view('cliente.dashboard',compact('orders'));
        }

    }
}
