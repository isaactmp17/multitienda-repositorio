<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Helper extends Model
{

    public static function getCities($id)
    {
        $items=City::where('state_id',$id)->select('id','name')->orderBy('name')->get();
        return $items;
    }


    // Cart Helpers
    public function getCart($store_id){
        $this->checkCart($store_id);
        $store=Store::where('id',$store_id)->first();
        $items=session('cart'.$store_id);
        $items_response=array();
        if(count($items)>0){
            foreach ($items as $item){
                $product=Product::where('id',$item['id'])->select('id','name','description','price','old_price','wholesaler_price','stock','slug','photo_1','type','is_active')->first();
                if($product->type=='s'){
                    array_push($items_response,['id'=>$item['id'],'type'=>$product->type,'quanty'=>$item['quanty'],'taxonomy'=>null,'available_stock'=>$product->stock,'details'=>$product]);
                }else{
                    $taxonomies_stock=json_decode($product->stock);
                    $max=0;
                    foreach($taxonomies_stock as $taxonomy_stock){
                        if($item['taxonomy']==$taxonomy_stock[0]){
                            $max=$taxonomy_stock[1];
                            break;
                        }
                    }
                    array_push($items_response,['id'=>$item['id'],'type'=>$product->type,'quanty'=>$item['quanty'],'taxonomy'=>$item['taxonomy'],'available_stock'=>$max,'details'=>$product]);
                }

            }
        }
        return (object) ['slug'=>$store->slug,'items'=>$items_response];
    }
    public function checkCart($store_id)
    {
        if(!session()->has('cart'.$store_id)){
            session(['cart'.$store_id=>array()]);
            if(session()->has('carts')){
                if(session('carts')!=$store_id) {
                    if (session()->has('cart' . session('carts'))) {
                        session()->forget('cart' . session('carts'));
                    }
                }
            }else{
                session(['carts'=>$store_id]);
            }
        }

    }

    public function addProductToCart($store_id,$product_id,$quanty,$product_taxonomy=null){
        $this->checkCart($store_id);
        $cart=session('cart'.$store_id);
        $item=Product::findOrFail($product_id);
        $stock_item=0;
        if($item->type=='s'){
            $stock_item=$item->stock;
        }else{
            foreach (json_decode($item->stock) as $taxonomy){
                if($taxonomy[0]==$product_taxonomy){
                    $stock_item=$taxonomy[1];
                    break;
                }
            }
        }
        if(count($cart)>0){
            for($i=0;$i<count($cart);$i++) {
                if($cart[$i]['id']==$product_id&&$cart[$i]['taxonomy']==$product_taxonomy){
                    $full_quanty=$cart[$i]['quanty']+$quanty;
                    if($full_quanty<=$stock_item){
                        $cart[$i]['quanty'] = $full_quanty;
                        session(['cart'.$store_id=>$cart]);
                        return ['band'=>1,'msg'=>'El producto ha sido agregado correctamente a la cesta.'];
                    }else{
                        return ['band'=>0,'msg'=>'No se puede agregar esta cantidad del producto, porque actualmente tiene '.$cart[$i]['quanty'].' en la cesta y la tienda tiene disponible '.$stock_item.' unidad(es).'];
                    }
                }
            }
            array_push($cart,['id'=>$item->id,'type'=>$item->type,'quanty'=>$quanty,'taxonomy'=>$product_taxonomy]);
            session(['cart'.$store_id=>$cart]);
            return ['band'=>1,'msg'=>'El producto ha sido agregado correctamente a la cesta.'];
        }else{
            if($quanty<=$stock_item){
                array_push($cart,['id'=>$item->id,'type'=>$item->type,'quanty'=>$quanty,'taxonomy'=>$product_taxonomy]);
                session(['cart'.$store_id=>$cart]);
                return ['band'=>1,'msg'=>'El producto ha sido agregado correctamente a la cesta.'];
            }
            return ['band'=>0,'msg'=>'El stock del producto ha cambiado a: '.$stock_item];
        }
    }

    public function checkStock($item,$quanty,$product_taxonomy){
        $stock_item=0;
        if($item->type=='s'){
            $stock_item=$item->stock;
        }else{
            foreach(json_decode($item->stock) as $taxonomy){
                if($taxonomy[0]==$product_taxonomy){
                    $stock_item=$taxonomy[1];
                    break;
                }
            }
        }

        return $quanty <= $stock_item ? true : false;
    }
}
