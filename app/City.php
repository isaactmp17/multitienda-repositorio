<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function user()
    {
        return $this->hasMany('App\User');
    }
    public function orders()
    {
        return $this->hasMany('App\Order');
    }
    public function state()
    {
        return $this->belongsTo('App\State','state_id');
    }
}
