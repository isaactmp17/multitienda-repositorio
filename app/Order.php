<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'order_reference', 'store_id', 'user_id','city_id','fullname','address','address','phone','reference_pol','payu_response','total_ammount','status',
    ];
    public function orderProducts()
    {
        return $this->hasMany('App\OrderProduct');
    }
    public function city()
    {
        return $this->belongsTo('App\City','city_id');
    }
    public function store()
    {
        return $this->belongsTo('App\Store','store_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
