<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = ['name','nit','photo','description','slug','tags','user_id','is_active'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function categories()
    {
        return $this->belongsToMany('App\Category', 'store_category');
    }
    public function products()
    {
        return $this->hasMany('App\Product');
    }
    public function orders()
    {
        return $this->hasMany('App\Order');
    }
}
