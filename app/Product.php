<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','description','price','old_price','wholesaler_price','stock','slug','keywords','store_id','product_size_id','type','photo_1','photo_2','photo_3','is_featured','is_active'];

    public function orderProducts()
    {
        return $this->hasMany('App\OrderProduct');
    }
    public function productSize()
    {
        return $this->belongsTo('App\ProductSize','product_size_id');
    }
    public function store()
    {
        return $this->belongsTo('App\Store','store_id');
    }
}
