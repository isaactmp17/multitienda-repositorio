<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'fullname', 'email', 'password','document_type','document_number','address','role_id','city_id','store_id','is_active',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function store()
    {
        return $this->hasOne('App\Store');
    }

    public function role()
    {
        return $this->belongsTo('App\Role','role_id');
    }
    public function city()
    {
        return $this->belongsTo('App\City','city_id');
    }
    public function orders()
    {
        return $this->hasMany('App\Order');
    }
}
