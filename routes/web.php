<?php

use App\Store;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('get/{action}/{id}', 'PagesController@getRequest');


Auth::routes();

Route::middleware(['check_status'])->group(function () {
    Route::get('/home', 'DashboardController@index')->name('home');

    // Cllient Routes
    Route::get('/payu/get/response', 'Cliente\CheckOutController@getPayu');
    Route::post('/tienda/{slug}/cart/checkout', 'Cliente\CheckOutController@checkOut');
    Route::get('/tienda/{slug}/order/{code}', 'Cliente\CheckOutController@viewOrder');


    // No store
    Route::get('manager/no_store',function (){
        if(Store::where('user_id',Auth::user()->id)->exists()) {
            return redirect()->to('home');
        }
        return view('errors.no_store');
    });
    //Store Routes
    Route::middleware(['manager'])->group(function () {
        Route::prefix('manager')->group(function () {


            // Productos
            Route::post('products/get_product_sizes', 'Manager\ProductController@getProductSizes');
            Route::post('products/update_status', 'Manager\ProductController@updateStatus');
            Route::post('products/update_featured', 'Manager\ProductController@updateFeatured');
            Route::resource('products', 'Manager\ProductController');
            Route::get('order/{code}', 'Manager\OrderController@index');
            Route::post('order/{code}/update_status', 'Manager\OrderController@updateStatus');
        });
    });
    //Admin Routes
    Route::middleware(['admin'])->group(function () {
        Route::prefix('admin')->group(function () {

            // Stores
            Route::post('stores/categories/{id}', 'Administrador\StoreController@updateStoreCategories');
            Route::get('stores/categories/{id}', 'Administrador\StoreController@storeCategories');
            Route::get('stores/categories', 'Administrador\StoreController@indexStoreCategories');
            Route::post('stores/update_status', 'Administrador\StoreController@updateStatus');
            Route::resource('stores', 'Administrador\StoreController');

            // Users
            Route::post('users/update_status', 'Administrador\UserController@updateStatus');
            Route::resource('users', 'Administrador\UserController');

            // Categories
            Route::post('categories/update_status', 'Administrador\CategoryController@updateStatus');
            Route::resource('categories', 'Administrador\CategoryController');

            // Product Sizes
            Route::post('product_sizes/update_status', 'Administrador\ProductSizesController@updateStatus');
            Route::resource('product_sizes', 'Administrador\ProductSizesController');
        });
    });

});

// Stores Routes General
Route::group(['middleware' => ['cors']], function () {
    Route::post('/payu/post/response', 'Cliente\CheckOutController@postPayu');
});
Route::get('/tienda/{slug}/cart/login', 'Cliente\CheckOutController@login');
Route::post('/tienda/{slug}/cart/login', 'Cliente\CheckOutController@loginPost');
Route::post('/tienda/{slug}/cart/register', 'Cliente\CheckOutController@register');
Route::get('/tienda/{slug}/cart', 'Cliente\StoreController@indexCart');
Route::post('/tienda/{slug}/cart/delete_item', 'Cliente\CheckOutController@deleteItem');
Route::get('/buscar', 'Cliente\StoreController@serchProducts');
Route::get('/categoria/{slug}', 'Cliente\StoreController@viewCategory');
Route::post('/add_to_cart/{slug}/{slug_product}', 'Cliente\StoreController@addToCart');
Route::get('/tienda/{slug}/producto/{slug_product}', 'Cliente\StoreController@viewProduct');
Route::get('/tienda/{slug}', 'Cliente\StoreController@index');



