@extends('layouts.app')
@section('page_title')
    {{$item->name.' | '.env('APP_NAME')}}
@endsection
@section('seo')
    <meta name="robots" content="index, follow">
    <meta name="description" content="{{$item->description?$item->description:env('APP_DESCRIPTION')}}">
@endsection
@section('head_styles')

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9 mt-4 mt-md-0">
                <div class="row tienda-header py-4">
                    <div class="col-12 my-3">
                        <h1 class="p-2">Tienda: #{{$item->name}}</h1>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <h3>Datos del comprador y entrega</h3>
                        <hr>
                        Por favor diligencie los datos solicitados.
                        @guest
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <?php
                                    $url='';
                                    if($email){
                                        if(!$passed){
                                            $url='register';
                                        }
                                    }

                                    ?>
                                    <form action="{{$url}}" method="{{$email?'POST':'GET'}}">
                                        @if($email)
                                            @csrf
                                        @endif
                                        <label class="d-block font-weight-bold mt-3">Email</label>
                                        <input type="email" name="email" value="{{$email?$email:''}}" class="form-control" {{$email?$passed?'':'readonly':''}} required>
                                        @error('email')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        @if($passed)
                                            <label class="d-block font-weight-bold mt-3">Contraseña</label>
                                            <input type="password" name="password" value="" class="form-control" required>
                                            @error('password')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        @else
                                            @if($email)
                                                <label class="d-block font-weight-bold mt-3">Contraseña</label>
                                                <input type="password" name="password" value="" class="form-control" required>
                                                @error('password')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                                <label class="d-block font-weight-bold mt-3">Confirmar contraseña</label>
                                                <input type="password" name="password_confirmation" value="" class="form-control" required>
                                                @error('password_confirmation')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                                <label class="d-block font-weight-bold mt-3">Nombre completo</label>
                                                <input type="text" name="name" value="{{old('name')}}" class="form-control" required>
                                                @error('name')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                                <label class="d-block font-weight-bold mt-3">Tipo de documento</label>
                                                <select name="document_type" class="form-control">
                                                    <option value="C.C." {{old('document_type')=='C.C.'?'selected':''}}>C.C.</option>
                                                    <option value="C.E." {{old('document_type')=='C.E.'?'selected':''}}>C.E.</option>
                                                    <option value="PASAPORTE" {{old('document_type')=='PASAPORTE'?'selected':''}}>PASAPORTE</option>
                                                </select>
                                                <label class="d-block font-weight-bold mt-3">Número de documento</label>
                                                <input type="text" name="document_number" value="{{old('document_number')}}" class="form-control" required>
                                                @error('document_number')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                                <div class="form-group mt-2">
                                                    <label class="font-weight-bold">Departamento</label>
                                                    <select name="state_id" id="state_id" class="form-control" searchable="Buscar...">
                                                        <option value="" disabled selected>Seleccione...</option>
                                                        @foreach($states as $state)
                                                            <option value="{{$state->id}}" {{isset($item)&&!is_null($item->city_id)?($item->city->state->id==$state->id)?'selected':'':''}}>{{$state->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @error('state_id')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                                <div class="form-group mt-2">
                                                    <label class="font-weight-bold">Ciudad</label>
                                                    <select name="city_id" id="city_id" class="form-control" searchable="Buscar...">
                                                        <option value="" disabled selected>---</option>
                                                    </select>
                                                </div>
                                                @error('city_id')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                                <label class="d-block font-weight-bold mt-3">Dirección</label>
                                                <input type="text" name="address" value="{{old('address')}}" class="form-control" required>
                                                @error('address')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            @endif
                                        @endif
                                        <a href="/tienda/{{$item->slug}}/cart" class="btn btn-secondary ml-0">Volver al carrito</a>
                                        <button type="submit" class="btn btn-primary ml-0 mt-2">Continuar</button>
                                    </form>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-md-8 mt-2">
                                    <form action="/tienda/{{$item->slug}}/cart/checkout" method="POST">
                                        @csrf
                                        <div class="alert alert-success">Ya falta poco, por favor confirme el monto y el lugar a donde enviaremos su pedido.</div>
                                        <label class="d-block font-weight-bold mt-3">Nombre de quién recibe</label>
                                        <input type="text" name="name" value="{{old('name',Auth::user()->fullname)}}" class="form-control" required>
                                        @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <div class="form-group mt-2">
                                            <label class="font-weight-bold">Departamento</label>
                                            <select name="state_id" id="state_id" class="form-control" searchable="Buscar...">
                                                <option value="" disabled selected>Seleccione...</option>
                                                @foreach($states as $state)
                                                    <option value="{{$state->id}}" {{Auth::check()&&!is_null(Auth::user()->city_id)?(Auth::user()->city->state->id==$state->id)?'selected':'':''}}>{{$state->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('state_id')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <div class="form-group mt-2">
                                            <label class="font-weight-bold">Ciudad</label>
                                            @if(!is_null(Auth::user()->city_id))
                                                <?php
                                                $cities=\App\Helper::getCities(Auth::user()->city->state->id);
                                                ?>
                                                <select name="city_id" id="city_id" class="form-control">
                                                    <option value="">Seleccione...</option>
                                                    @foreach($cities as $city)
                                                        <option value="{{$city->id}}" {{Auth::check()?(Auth::user()->city_id==$city->id)?'selected':'':(old('city_id')==$city->id)?'selected':''}}>{{$city->name}}</option>
                                                    @endforeach
                                                </select>
                                            @else
                                                <select name="city_id" id="city_id" class="mdb-select md-form mt-1" searchable="Buscar...">
                                                    <option value="" disabled selected>---</option>
                                                </select>
                                            @endif
                                        </div>
                                        @error('city_id')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <label class="d-block font-weight-bold mt-3">Dirección de envío</label>
                                        <input type="text" name="address" value="{{old('address',Auth::user()->address)}}" class="form-control" required>
                                        @error('address')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <label class="d-block font-weight-bold mt-3">Teléfono</label>
                                        <input type="tel" name="phone" value="{{old('phone')}}" class="form-control" required>
                                        @error('phone')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <?php
                                            $total_ammount=0;
                                        ?>
                                        @foreach($shop_cart_items->items as $ind=>$itemc)
                                        <?php
                                            $price_unit=0;
                                            $total_items=0;
                                            if($itemc['quanty']>5){
                                                $price_unit=$itemc['details']->wholesaler_price;
                                            }else{
                                                $price_unit=$itemc['details']->price;
                                            }
                                            $total_items=$price_unit*$itemc['quanty'];
                                            $total_ammount+=$total_items;
                                        ?>
                                        @endforeach
                                        <h4 class="font-weight-bold mt-4">Total a pagar: {{number_format($total_ammount,0,',','.')}} COP</h4>
                                        <a href="/tienda/{{$item->slug}}/cart" class="btn btn-secondary ml-0">Volver al carrito</a>
                                        <button type="submit" class="btn btn-primary ml-0">Pagar</button>
                                    </form>
                                </div>
                            </div>

                        @endif
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <ul class="nav flex-column lighten-4 columna-categorias">
                    <li class="cabecera-columna">
                        Categorías
                    </li>
                    @foreach($categories as $category)
                        <li class="nav-item text-left">
                            <a class="nav-link" href="/categoria/{{$category->slug}}">{{$category->name}}</a>
                        </li>
                    @endforeach
                </ul>
                <img src="{{asset('statics/img/banners/banner-home-bot-lg-1.jpg')}}" class="img-fluid mt-2" alt="">
            </div>
        </div>

    </div>
@endsection
@section('footer_scripts')
    <script>
        $(document).ready(function() {
            $('#state_id').on('change',function(){
                $.ajax({
                    data:'',
                    url:'/get/cities/'+$(this).val(),
                    type:'GET',
                    beforeSend:function(){
                        swal({
                            title: 'Espere un momento por favor',
                            text:'Estamos cargando las ciudades',
                            icon:'info',
                            buttons: false,
                            closeOnEsc: false,
                            closeOnClickOutside: false
                        })
                    },
                    success:function (response) {
                        $('#city_id').html('');
                        $.each(response,function(index,value){
                            $('#city_id').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                        swal({
                            title: 'Excelente',
                            text:'Ciudades cargadas correctamente.',
                            icon:'success',
                            closeOnEsc: false,
                            closeOnClickOutside: false
                        })
                    }
                })
            });
        });
    </script>
@endsection
