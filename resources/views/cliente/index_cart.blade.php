@extends('layouts.app')
@section('page_title')
    {{$item->name.' | '.env('APP_NAME')}}
@endsection
@section('seo')
    <meta name="robots" content="index, follow">
    <meta name="description" content="{{$item->description?$item->description:env('APP_DESCRIPTION')}}">
@endsection
@section('head_styles')

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9 mt-4 mt-md-0">
                <div class="row tienda-header py-4">
                    <div class="col-12 my-3">
                        <h1 class="p-2">Tienda: #{{$item->name}}</h1>
                    </div>
                </div>
                @if(count($shop_cart_items->items)>0)
                    <div class="row mt-4">
                        <div class="col-12">
                            <h3>Carrito de compras</h3>
                            <hr>
                            <?php
                                $total_ammount=0;
                            ?>
                            @foreach($shop_cart_items->items as $ind=>$itemc)
                                <div class="row {{$ind>0?'mt-3':''}}">
                                    <div class="col-3">
                                        <img class="d-block w-100" src="{{asset('statics/img/products/'.$itemc['details']->id.'/photo_1_full.'.$itemc['details']->photo_1.'?v='.strtotime($itemc['details']->updated_at))}}"
                                             alt="{{$itemc['details']->name}} 1">
                                    </div>
                                    <div class="col-9">
                                        <h5 class="h5-responsive font-weight-bold d-block">{{$itemc['details']->name}} {{$itemc['taxonomy']?' - '.$itemc['taxonomy']:''}}</h5>
                                        <small class="d-block">{{$itemc['details']->description}}</small>
                                        <div class="row">
                                            <div class="col-8">
                                                @if($itemc['details']->is_active==1)
                                                    @if($itemc['available_stock']>0)
                                                        <?php
                                                            $price_unit=0;
                                                            $total_items=0;
                                                            if($itemc['quanty']>5){
                                                                $price_unit=$itemc['details']->wholesaler_price;
                                                            }else{
                                                                $price_unit=$itemc['details']->price;
                                                            }
                                                            $total_items=$price_unit*$itemc['quanty'];
                                                            $total_ammount+=$total_items;
                                                        ?>
                                                        Costo unitario: {{number_format($itemc['quanty']>5?$itemc['details']->wholesaler_price:$itemc['details']->price,0,',','.')}} COP<br>
                                                        Seleccionó {{$itemc['quanty']}} de {{$itemc['available_stock']}} disponible(s).<br>
                                                        <strong>Total: {{number_format($total_items,0,',','.')}} COP</strong>

                                                        <div class="input-group">
                                                            <div class="input-group-append" id="">
                                                                <a href="{{asset('/tienda/'.$item->slug.'/producto/'.$itemc['details']->slug)}}" class="btn btn-md btn-warning m-0 px-3">Ver Producto</a>
                                                                <button class="btn btn-md btn-danger m-0 px-3 remove-product" type="button" data-target="{{$itemc['id']}}" data-taxonomy="{{$itemc['taxonomy']}}">Eliminar</button>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="alert alert-danger" role="alert">
                                                        Producto no disponible. Por favor elímenelo del carrito para continuar con su pedido.<br><button class="btn btn-md btn-danger m-0 px-3 remove-product" type="button" data-target="{{$itemc['id']}}" data-taxonomy="{{$itemc['taxonomy']}}">Eliminar</button>
                                                        </div>
                                                    @endif
                                                @else
                                                    <div class="alert alert-danger" role="alert">
                                                        Producto no disponible. Por favor elímenelo del carrito para continuar con su pedido.<br><button class="btn btn-md btn-danger m-0 px-3 remove-product" type="button" data-target="{{$itemc['id']}}" data-taxonomy="{{$itemc['taxonomy']}}">Eliminar</button>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        @if($itemc['available_stock']>0)
                                            @if($itemc['quanty']>$itemc['available_stock'])
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="alert alert-danger mt-2">La cantidad solicitada del producto <strong>{{$itemc['details']->name}} {{$itemc['taxonomy']?' - '.$itemc['taxonomy']:''}}</strong> supera la cantidad disponible. Productos actualmente en existencia: {{$itemc['available_stock']}}. Por favor elimínelo y vuelva a agregar los productos a la cesta.</div>
                                                </div>
                                            </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>

                            @endforeach
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12 text-right"><h4 class="font-weight-bold">Total a pagar: {{number_format($total_ammount,0,',','.')}} COP</h4></div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12 text-right">
                            <a class="btn btn-secondary" href="{{asset('/tienda/'.$item->slug)}}">Volver a la tienda</a>
                            <a class="btn btn-primary mr-0" href="{{asset('/tienda/'.$item->slug.'/cart/login')}}">Continuar</a>
                        </div>
                    </div>
                @else
                    <div class="row mt-4">
                        <div class="col-12 text-right">
                            <h3>No se han encontrado productos en la cesta.</h3>
                            <a class="btn btn-secondary" href="{{asset('/tienda/'.$item->slug)}}">Volver a la tienda</a>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-12 col-md-3">
                <ul class="nav flex-column lighten-4 columna-categorias">
                    <li class="cabecera-columna">
                        Categorías
                    </li>
                    @foreach($categories as $category)
                        <li class="nav-item text-left">
                            <a class="nav-link" href="/categoria/{{$category->slug}}">{{$category->name}}</a>
                        </li>
                    @endforeach
                </ul>
                <img src="{{asset('statics/img/banners/banner-home-bot-lg-1.jpg')}}" class="img-fluid mt-2" alt="">
            </div>
        </div>

    </div>
@endsection

