@extends('layouts.app')
@section('page_title')
    {{$item->name.' | '.env('APP_NAME')}}
@endsection
@section('seo')
    <meta name="robots" content="index, follow">
    <meta name="description" content="{{$item->description?$item->description:env('APP_DESCRIPTION')}}">
@endsection
@section('head_styles')

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9 mt-4 mt-md-0">
                <div class="row tienda-header py-4">
                    <div class="col-12 my-3">
                        <h1 class="p-2">Categoría: #{{$item->name}}</h1>
                    </div>
                </div>
                <div class="row mt-4 {{$stores->count()>0?'mb-3':''}}">
                    <div class="col-12">
                        <h3 class="h3-responsive">Tiendas</h3>
                        <hr>
                    </div>
                </div>
                @if($stores->count()>0)
                    <div class="row mb-3">
                        @foreach($stores as $store)
                                <div class="col-12 col-md-6">
                                    <div class="row">
                                        <div class="col-md-4 text-center mt-3">
                                            <img src="{{asset('statics/img/stores/'.$store->id.'.'.$store->photo.'?v='.strtotime($store->updated_at))}}" class="img-fluid">
                                        </div>
                                        <div class="col-md-8 mt-3">
                                            <h5 class="h5-responsive text-center text-md-left d-block">{{$store->name}}</h5>
                                            <small class="d-block  text-center text-md-left">
                                                {{$store->description?$store->description:''}}
                                            </small>
                                            <div class="text-center text-md-left">
                                                <a class="btn btn-primary btn-sm ml-0" href="{{asset('/tienda/'.$store->slug)}}">Ver Tienda</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                        @endforeach

                    </div>
                @else
                <div class="row">
                    <div class="col-12">No hay tiendas dentro de esta categoría.</div>
                </div>
                @endif
            </div>

            <div class="col-12 col-md-3">
                <ul class="nav flex-column lighten-4 columna-categorias">
                    <li class="cabecera-columna">
                        Categorías
                    </li>
                    @foreach($categories as $category)
                        <li class="nav-item text-left">
                            <a class="nav-link {{$category->id==$item->id?'active':''}}" href="/categoria/{{$category->slug}}">{{$category->name}}</a>
                        </li>
                    @endforeach
                </ul>
                <img src="{{asset('statics/img/banners/banner-home-bot-lg-1.jpg')}}" class="img-fluid mt-2" alt="">
            </div>
        </div>

    </div>
@endsection

