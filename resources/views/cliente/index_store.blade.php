@extends('layouts.app')
@section('page_title')
    {{$item->name.' | '.env('APP_NAME')}}
@endsection
@section('seo')
    <meta name="robots" content="index, follow">
    <meta name="description" content="{{$item->description?$item->description:env('APP_DESCRIPTION')}}">
@endsection
@section('head_styles')

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9 mt-4 mt-md-0">
                <div class="row tienda-header py-4">
                    <div class="col-12 my-3">
                        <h1 class="p-2">Tienda: #{{$item->name}}</h1>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 text-center d-none d-md-block">
                <img src="{{asset('statics/img/stores/'.$item->id.'.'.$item->photo.'?v='.strtotime($item->updated_at))}}" alt="{{$item->name}}" class="w-75">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-9 mt-4 mt-md-0">
                @if(count($products_featured)>0)
                <div class="row mt-4">
                    <div class="col-12">
                        <h3 class="h3-responsive">Productos más vendidos</h3>
                    </div>
                </div>
                <div class="row">
                    @foreach($products_featured as $product_featured)
                        <div class="col-6 col-md-3 text-center">
                            <img src="{{asset('statics/img/products/'.$product_featured->id.'/photo_1_270x370.'.$product_featured->photo_1.'?v='.strtotime($product_featured->updated_at))}}" class="img-fluid" alt="{{$product_featured->name}}">
                            <span class="d-block mt-2">{{$product_featured->name}}</span>
                            <span class="d-block font-weight-bold text-primary">${{number_format($product_featured->price,0,',','.')}}</span>
                            <a class="btn btn-primary btn-sm" href="{{asset('/tienda/'.$product_featured->store->slug.'/producto/'.$product_featured->slug)}}">Ver Producto</a>
                        </div>
                    @endforeach
                </div>
                <hr>
                @endif
                @if(count($products)>0)
                    {!! count($products_featured)>0?'Productos':'<h3 class="h3-responsive mt-4">Productos</h3>' !!}

                    <div class="row">
                        @foreach($products as $product)
                            <div class="col-6 col-md-3 text-center mt-3">
                                <img src="{{asset('statics/img/products/'.$product->id.'/photo_1_270x260.'.$product->photo_1.'?v='.strtotime($product->updated_at))}}" class="img-fluid" alt="{{$product->name}}">
                                <span class="d-block mt-2">{{$product->name}}</span>
                                <span class="d-block font-weight-bold text-primary">${{number_format($product->price,0,',','.')}}</span>
                                <a class="btn btn-primary btn-sm" href="{{asset('/tienda/'.$item->slug.'/producto/'.$product->slug)}}">Ver Producto</a>
                            </div>
                        @endforeach
                    </div>
                    <div class="row mt-4">
                        <div class="col-12 ">
                            <span class="d-block float-right">
                                {{ $products->links() }}
                            </span>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-12 col-md-3">
                <ul class="nav flex-column lighten-4 columna-categorias">
                    <li class="cabecera-columna">
                        Categorías
                    </li>
                    @foreach($categories as $category)
                        <li class="nav-item text-left">
                            <a class="nav-link" href="/categoria/{{$category->slug}}">{{$category->name}}</a>
                        </li>
                    @endforeach
                </ul>
                <img src="{{asset('statics/img/banners/banner-home-bot-lg-1.jpg')}}" class="img-fluid mt-2" alt="">
            </div>
        </div>

    </div>
@endsection

