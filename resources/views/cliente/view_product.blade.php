@extends('layouts.app')
@section('page_title')
    {{$item->name.' | '.$store->name.' | '.env('APP_NAME')}}
@endsection
@section('seo')
    <meta name="robots" content="index, follow">
    <meta name="description" content="{{$item->description?$item->description:env('APP_DESCRIPTION')}}">
@endsection
@section('head_styles')
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9 mt-4 mt-md-0">
                <div class="row tienda-header py-4">
                    <div class="col-12 my-3">
                        <h1 class="p-2">Tienda: #{{$store->name}}</h1>
                        <h1 class="p-2">Producto: #{{$item->name}}</h1>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-6">
                        <!--Carousel Wrapper-->
                        <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
                            <!--Slides-->
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="{{asset('statics/img/products/'.$item->id.'/photo_1_full.'.$item->photo_1.'?v='.strtotime($item->updated_at))}}"
                                         alt="{{$item->name}} 1">
                                </div>
                                @if(!is_null($item->photo_2))
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="{{asset('statics/img/products/'.$item->id.'/photo_2_full.'.$item->photo_2.'?v='.strtotime($item->updated_at))}}"
                                         alt="{{$item->name}} 2">
                                </div>
                                @endif
                                @if(!is_null($item->photo_3))
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="{{asset('statics/img/products/'.$item->id.'/photo_3_full.'.$item->photo_3.'?v='.strtotime($item->updated_at))}}"
                                         alt="{{$item->name}} 3">
                                </div>
                                @endif
                            </div>
                            <!--/.Slides-->
                            <!--Controls-->
                            <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            <!--/.Controls-->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-thumb" data-slide-to="0" class="active">
                                    <img src="{{asset('statics/img/products/'.$item->id.'/photo_1_270x260.'.$item->photo_1.'?v='.strtotime($item->updated_at))}}" width="100">
                                </li>
                                @if(!is_null($item->photo_2))
                                <li data-target="#carousel-thumb" data-slide-to="1">
                                    <img src="{{asset('statics/img/products/'.$item->id.'/photo_2_270x260.'.$item->photo_2.'?v='.strtotime($item->updated_at))}}" width="100">
                                </li>
                                @endif
                                @if(!is_null($item->photo_3))
                                <li data-target="#carousel-thumb" data-slide-to="2">
                                    <img src="{{asset('statics/img/products/'.$item->id.'/photo_3_270x260.'.$item->photo_3.'?v='.strtotime($item->updated_at))}}" width="100">
                                </li>
                                @endif
                            </ol>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4 class="h4-responsive d-block"><strong>Nombre del Producto:</strong> #{{$item->name}}</h4>
                        <h4 class="h4-responsive d-block mt-2"><strong>Precio:</strong> ${{number_format($item->price,0,'','.')}}. @if(!is_null($item->old_price))<small class="text-danger" style="font-size: .65em">Antes <span style="text-decoration:line-through;">${{number_format($item->old_price,0,'','.')}}</span></small>@endif</h4>
                        @if(!is_null($item->wholesaler_price))<h4 class="h4-responsive d-block mt-2"><strong>Precio mayorista:</strong> ${{number_format($item->wholesaler_price,0,'','.')}}.</h4>@endif
                        <h4 class="h4-responsive d-block font-weight-bold">
                            Descripción:
                        </h4>
                        <p>
                            {!! nl2br($item->description) !!}
                        </p>
                        <input type="hidden" id="price" value="{{number_format($item->price,0,'','')}}">
                        <input type="hidden" id="wholesaler_price" value="{{!is_null($item->wholesaler_price)?number_format($item->wholesaler_price,0,'',''):number_format($item->price,0,'','')}}">
                        <form action="{{asset('add_to_cart/'.$store->slug.'/'.$item->slug)}}" method="POST">
                            @csrf
                            <input type="hidden" name="store_id" value="{{$store->id}}">
                            <input type="hidden" name="product_id" value="{{$item->id}}">
                            <?php $band=0; ?>
                            @if($item->type=='s')
                                @if($item->stock>0)
                                    <?php $band=1; ?>
                                    <div class="row">
                                        <div class="col-6 font-weight-bold">Cant.</div>
                                        <div class="col-6">
                                            <div class="input-group">
                                                <input type="text" name="quanty" data-max="{{ session('error_stock')?session('error_stock')['stock']:$item->stock }}" id="quanty" class="form-control" value="{{old('quanty',0)}}" readonly onclick="swal({title:'',text:'Utilice los botones',icon:'warning'})">
                                                <div class="input-group-append" id="MaterialButton-addon4">
                                                    <button class="btn btn-md btn-primary m-0 px-3" type="button" id="add-quanty"><i class="fa fa-plus"></i></button>
                                                    <button class="btn btn-md btn-secondary m-0 px-3" type="button" id="remove-quanty"><i class="fas fa-minus"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    No hay unidades disponibles.
                                @endif
                                @error('quanty')
                                <div class="alert alert-danger mt-2">{{ $message }}</div>
                                @enderror
                                @if(!session('error_stock'))
                                    <div class="row mt-4">
                                        <div class="col-6 font-weight-bold">Unid. disponibles</div>
                                        <div class="col-6">
                                            {{ $item->stock }}
                                        </div>
                                    </div>
                                @else
                                    <div class="alert alert-danger mt-2">{{session('error_stock')['msg']}}</div>
                                @endif
                            @else
                                <?php
                                    $band=0;
                                    foreach (json_decode($item->stock) as $stock){
                                        if($stock[1]>0){
                                            $band=1;
                                            break;
                                        }
                                    }
                                ?>
                            @if($band==1)
                                <div class="row">
                                    <div class="col-6 font-weight-bold">{{$item->productSize->name}}</div>
                                    <div class="col-6">
                                        <?php $available=0; ?>
                                        <select name="product_taxonomy" id="product_taxonomy" class="form-control">
                                            @foreach(json_decode($item->stock) as $stock)
                                                @if($stock[1]>0)
                                                    <?php if($available==0){$available=$stock[1];} ?>
                                                    <option value="{{$stock[0]}}">{{$stock[0]}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-6 font-weight-bold">Cant.</div>
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input type="text" name="quanty" data-max="{{ session('error_stock')?session('error_stock')['stock']:$available }}" id="quanty" class="form-control" value="{{old('quanty',0)}}" readonly onclick="swal({title:'',text:'Utilice los botones',icon:'warning'})">
                                            <div class="input-group-append" id="MaterialButton-addon4">
                                                <button class="btn btn-md btn-primary m-0 px-3" type="button" id="add-quanty"><i class="fa fa-plus"></i></button>
                                                <button class="btn btn-md btn-secondary m-0 px-3" type="button" id="remove-quanty"><i class="fas fa-minus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                No hay unidades disponibles.
                            @endif
                                <table class="table table-striped mt-4">
                                    <thead>
                                    <tr>
                                        <th class="font-weight-bold" colspan="2">Unidades disponibles</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(json_decode($item->stock) as $stock)
                                            <tr>
                                                <td class="font-weight-bold">
                                                    {{$stock[0]}}
                                                </td>
                                                <td class="stock_value" data-target="{{$stock[0]}}">
                                                    {{$stock[1]}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                            <div class="d-block text-right mt-4">
                                <a class="btn btn-secondary" type="button" href="{{asset('/tienda/'.$store->slug)}}">Ver tienda</a>
                                <button class="btn btn-primary {{$band==1?'':'disabled'}}" type="submit">Añadir al Carrito</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-3">
                <ul class="nav flex-column lighten-4 columna-categorias">
                    <li class="cabecera-columna">
                        Categorías
                    </li>
                    @foreach($categories as $category)
                        <li class="nav-item text-left">
                            <a class="nav-link" href="/categoria/{{$category->slug}}">{{$category->name}}</a>
                        </li>
                    @endforeach
                </ul>
                <img src="{{asset('statics/img/banners/banner-home-bot-lg-1.jpg')}}" class="img-fluid mt-2" alt="">
            </div>
        </div>

    </div>
@endsection

@section('footer_scripts')
    <script type="text/javascript">
        $(function (){
            var loadQuanty = function (band){
                if(band){
                    var taxonomy=$('#product_taxonomy').val();
                    $.each($('.stock_value'),function (index,value){
                        if($(this).data('target')==taxonomy){
                            $('#quanty').data('max',parseInt($(this).html()));
                        }
                    })
                }

            }
            $('#product_taxonomy').on('change',function (){
                loadQuanty(true);
                $('#quanty').val(0);
            })
        @if(session('error_stock')||session('error'))
            loadQuanty(false);
        @else
            loadQuanty(true);
        @endif
        })
    </script>

@endsection

