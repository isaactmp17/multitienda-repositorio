@extends('layouts.app')
@section('head_styles')

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9 mt-4 mt-md-0">
                <div class="row tienda-header py-4">
                    <div class="col-12 my-3">
                        <h1 class="p-2">Resultados de la búsqueda</h1>
                    </div>
                </div>

                <div class="row mt-4">
                    @if($items->count()>0)
                        @foreach($items as $index=>$item)
                                <div class="col-6 col-md-3 text-center mt-3">
                                    <img src="{{asset('statics/img/products/'.$item->id.'/photo_1_270x260.'.$item->photo_1.'?v='.strtotime($item->updated_at))}}" class="img-fluid" alt="{{$item->name}}">
                                    <span class="d-block mt-2">{{$item->name}}</span>
                                    <span class="d-block font-weight-bold text-primary">${{number_format($item->price,0,',','.')}}</span>
                                    <a class="btn btn-primary btn-sm" href="{{asset('/tienda/'.$item->store->slug.'/producto/'.$item->slug)}}">Ver Producto</a>
                                </div>
                        @endforeach
                    @else
                            <div class="col-12">
                                <h4>No se ha encontrado ningún producto.</h4>
                                <br>
                                <a href="/" class="btn btn-primary ml-0">Volver atrás</a>
                            </div>

                    @endif
                </div>
                @if($items->count()>0)
                    <div class="row mt-4">
                        <div class="col-12 ">
                                <span class="d-block float-right">
                                    {{ $items->appends(['q'=>$query])->links() }}
                                </span>
                        </div>
                    </div>
                @endif
            </div>

            <div class="col-12 col-md-3">
                <ul class="nav flex-column lighten-4 columna-categorias">
                    <li class="cabecera-columna">
                        Categorías
                    </li>
                    @foreach($categories as $category)
                        <li class="nav-item text-left">
                            <a class="nav-link" href="/categoria/{{$category->slug}}">{{$category->name}}</a>
                        </li>
                    @endforeach
                </ul>
                <img src="{{asset('statics/img/banners/banner-home-bot-lg-1.jpg')}}" class="img-fluid mt-2" alt="">
            </div>
        </div>

    </div>
@endsection

@section('footer_scripts')
@endsection

