@extends('layouts.app_dashboard')

@section('content')
@if($order->status==1)
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h4 class="card-title font-weight-bold">Espere un momento por favor...</h4>
                        <img src="{{asset('/img/preloader.gif')}}" height="40px" alt="">
                    </div>
                    <div class="card-footer text-center">
                        Lo estamos redireccionando a la pasarela de pago
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form method="post" id="formPayu" action="https://checkout.payulatam.com/ppp-web-gateway-payu/">
        <input name="merchantId"    type="hidden"  value="892084" >
        <input name="accountId"     type="hidden"  value="898598" >
        <input name="buyerFullName" type="hidden"  value="{{Auth::user()->fullname}}" >
        <input name="description"   type="hidden"  value="Pago del pedido: {{$order->order_reference}}. Tienda: {{$item->name}}."  >
        <input name="referenceCode" type="hidden"  value="{{$order->order_reference}}" >
        <input name="amount"        type="hidden"  value="{{$order->total_ammount}}"   >
        <input name="tax"           type="hidden"  value="0" >
        <input name="taxReturnBase" type="hidden"  value="0" >
        <input name="currency"      type="hidden"  value="COP" >
        <?php
            //ApiKey~merchantId~referenceCode~amount~currency
        ?>
        <input name="signature"     type="hidden"  value="{{md5('J3glV63gOKljXFOPd14YEw0hdi~892084~'.$order->order_reference.'~'.$order->total_ammount.'~COP')}}"  >
        <input name="test"          type="hidden"  value="0" >
        <input name="buyerEmail"    type="hidden"  value="{{Auth::user()->email}}" >
        <input name="responseUrl"    type="hidden"  value="{{asset('/payu/get/response')}}" >
        <input name="confirmationUrl"    type="hidden"  value="{{asset('/payu/post/response')}}" >
    </form>
@else

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row tienda-header py-4">
                    <div class="col-12 my-3">
                        <h1 class="p-2">Pedido: {{$order->order_reference}}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 mt-4">
                <div class="card card-primary">
                    <div class="card-header font-weight-bold">
                        Información del pedido
                    </div>
                    <div class="card-body">
                        <strong>Nº Pedido</strong><br>
                        {{$order->order_reference}}<br><br>
                        <strong>Fecha del pedido</strong><br>
                        {{date('d-m-Y h:i A',strtotime($order->created_at))}}<br><br>
                        <strong>Nombre de quién recibe</strong><br>
                        {{$order->fullname}}<br><br>
                        <strong>Dirección de quién recibe</strong><br>
                        {{$order->address}}<br><br>
                        <strong>Ciudad, Departamento.</strong><br>
                        {{$order->city->name.', '.$order->city->state->name}}<br><br>
                        <strong>Teléfono de contacto</strong><br>
                        {{$order->phone}}<br><br>
                        <strong>Tienda</strong><br>
                        {{$order->store->name}}<br><br>
                        <strong>Estado del pedido</strong><br>
                        <?php
                            if($order->status==0){
                                $status="Anulado";
                            }elseif($order->status==1){
                                $status="Pendiente de pago";
                            }elseif($order->status==2){
                                $status="Empacando su pedido";
                            }elseif($order->status==3){
                                $status="Pedido despachado";
                            }else{
                                $status="Pedido entregado";
                            }
                        ?>
                        {{$status}}
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-secondary btn-block" href="/home">Volver atrás</a>
                    </div>
                </div>
            </div>
            <div class="col-md-8 mt-4">
                <div class="card card-primary">
                    <div class="card-header font-weight-bold">
                        Detalles del pedido
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th class="text-center">Cantidad</th>
                                    <th class="text-center">Precio unitario</th>
                                    <th class="text-right">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $total_ammount=0;
                                ?>
                                @foreach($order->orderProducts as $product)
                                    <?php
                                        $total_ammount+=$product->quanty*$product->price;
                                    ?>
                                    <tr>
                                        <td>{{$product->name}} {{is_null($product->taxonomy)?'':'- '.$product->taxonomy}}</td>
                                        <td align="center">{{$product->quanty}}</td>
                                        <td align="center">{{number_format($product->price,0,',','.')}} COP</td>
                                        <td align="right">{{number_format($product->quanty*$product->price,0,',','.')}} COP</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3" align="right" class="font-weight-bold"> Total a pagar:</td>
                                    <td align="right">{{number_format($total_ammount,0,',','.')}} COP</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@endsection

@section('footer_scripts')
    @if($order->status==1)
        <script>
            $(function (){
                $('#formPayu').submit();
            })
        </script>
    @endif
@endsection
