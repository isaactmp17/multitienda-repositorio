@extends('layouts.app_dashboard')

@section('head_styles')
    <!--<link href="{{asset('css/pages/index_stores.css')}}" rel="stylesheet">-->
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <form action="{{(isset($item))?asset('admin/'.$uri.'/'.$item->id):asset('admin/'.$uri)}}"  enctype="multipart/form-data" method="POST">
                @csrf
                @isset($item)
                    @method('put')
                @endif
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="h4-responsive"><i class="fas fa-grip-vertical"></i> Crear Categoría</h4>
                    </div>
                    <div class="card-body">
                        <label class="d-block mt-3">Nombre <i class="fas fa-asterisk text-danger text-smaller"></i></label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', isset($item) ? $item->name : null) }}">
                        @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group mt-2">
                            <label>Descripción corta</label>
                            <textarea class="form-control rounded-0 @error('description') is-invalid @enderror" name="description" rows="3">{{ old('description', isset($item) ? $item->description : null) }}</textarea>
                        </div>
                        @error('description')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <br><br>
                        <i class="fas fa-asterisk text-danger text-smaller"></i> Campos obligatorios
                    </div>
                    <div class="card-footer text-right">
                        <a href="{{asset('admin/'.$uri)}}" class="btn btn-secondary">Volver atrás</a>
                        <button class="btn btn-primary">{{isset($item)?'Guardar':'Crear'}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection

@section('footer_scripts')
    <!-- <script type="text/javascript" src="{{ asset('js/pages/index_stores.js') }}"></script>-->
@endsection
