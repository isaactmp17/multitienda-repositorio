@extends('layouts.app_dashboard')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card card-cascade narrower">
                <div class="view view-cascade gradient-card-header secondary-color">
                    <h3 class="h3-responsive card-header-title">Ventas últimos 10 días</h3>
                </div>
                <div class="card-body pb-0">
                    <div class="d-flex justify-content-between">
                        <p class="display-4 align-self-end">{{count($orders)}}</p>
                    </div>
                    <canvas id="lineChart1" class="w-100" height="250px"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-cascade narrower">
                <div class="view view-cascade gradient-card-header secondary-color">
                    <h3 class="h3-responsive card-header-title">Productos más vendidos</h3>
                </div>
                <div class="card-body pb-0">
                    <div class="table-responsive text-nowrap">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($top_products as $index=>$top_product)
                                <tr>
                                    <th scope="row">{{$index+1}}</th>
                                    <td>{{$top_product->product->name}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer_scripts')
    <script>
        var ctxL = document.getElementById("lineChart1").getContext('2d');
        var myLineChart = new Chart(ctxL, {
            type: 'line',
            data: {
                labels: [
                    @for($i=10;$i>=0;$i--)
                    "{{now()->subDays($i)->toDateString()}}",
                    @endfor
                    ],
                <?php
                  $top=5;
                ?>
                datasets: [{
                    fill: false,
                    borderColor: "#673ab7",
                    pointBackgroundColor: "#673ab7",
                    data: [
                        @for($i=10;$i>=0;$i--)
                            <?php
                                $totalday=\App\Order::whereDate('created_at', '=', now()->subDays($i)->toDateString())->count();

                                if($totalday>$top)
                                    $top=$totalday;
                            ?>
                        "{{$totalday}}",
                        @endfor
                    ]
                }]
            },
            options: {
                responsive: false,
                legend: {
                    display: false
                },
                elements: {
                    line: {
                        tension: 0.0
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false,
                        },
                        ticks: {
                            padding: 15,
                            height: 30
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            drawBorder: false
                        },
                        ticks: {
                            maxTicksLimit: 5,
                            padding: 15,
                            min: 0,
                            max: {{$top}}
                        }
                    }]
                }
            }
        });
    </script>
@endsection
