@extends('layouts.app_dashboard')

@section('head_styles')
    <!--<link href="{{asset('css/pages/index_stores.css')}}" rel="stylesheet">-->
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <form action="{{(isset($item))?asset('admin/'.$uri.'/'.$item->id):asset('admin/'.$uri)}}"  enctype="multipart/form-data" method="POST">
                @csrf
                @isset($item)
                    @method('put')
                @endif
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="h4-responsive"><i class="fa fa-store"></i> Crear Tienda</h4>
                    </div>
                    <div class="card-body">
                        @isset($item)
                        <div class="row justify-content-center">
                            <div class="col-md-4 text-center">
                                Logo actual
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-4 text-center">
                                <img src="{{asset('statics/img/stores/'.$item->id.'.'.$item->photo.'?v='.time())}}" class="img-fluid" alt="">
                            </div>
                        </div>
                        @endif
                        <label class="d-block">Logo @if(!isset($item)) <i class="fas fa-asterisk text-danger text-smaller"></i> @endif</label>
                        <div class="custom-file">
                            <input type="file" name="logo" class="custom-file-input @error('logo') is-invalid @enderror" id="customFileLang" lang="es" value="{{ old('logo') }}">
                            <label class="custom-file-label" for="customFileLang">Seleccionar Archivo</label>
                        </div>
                        @error('logo')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <label class="d-block mt-3">Nombre <i class="fas fa-asterisk text-danger text-smaller"></i></label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', isset($item) ? $item->name : null) }}">
                        @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <label class="d-block mt-2">Nit</label>
                        <input type="text" name="nit" class="form-control @error('nit') is-invalid @enderror" value="{{ old('nit', isset($item) ? $item->nit : null) }}">
                        @error('nit')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group mt-2">
                            <label>Descripción corta</label>
                            <textarea class="form-control rounded-0 @error('description') is-invalid @enderror" name="description" rows="3">{{ old('description', isset($item) ? $item->description : null) }}</textarea>
                        </div>
                        @error('description')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group mt-2">
                            <label>Dueño</label>
                            <select name="user_id" class="form-control">
                                <option value="">Seleccione...</option>
                                @foreach($users as $user)
                                    <option value="{{$user->id}}" {{isset($item)?($item->user_id==$user->id)?'selected':'':(old('user_id')==$user->id)?'selected':''}}>{{$user->fullname}}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('user_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <br><br>
                        <i class="fas fa-asterisk text-danger text-smaller"></i> Campos obligatorios
                    </div>
                    <div class="card-footer text-right">
                        <a href="{{asset('admin/'.$uri)}}" class="btn btn-secondary">Volver atrás</a>
                        <button class="btn btn-primary">{{isset($item)?'Guardar':'Crear'}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection

@section('footer_scripts')
    <!-- <script type="text/javascript" src="{{ asset('js/pages/index_stores.js') }}"></script>-->
@endsection
