@extends('layouts.app_dashboard')

@section('head_styles')
    <link href="{{asset('css/pages/admin_index_stores.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 text-right mb-4">
            <a href="{{asset('admin/'.$uri.'/create')}}" class="btn btn-primary">Crear tienda</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-cascade narrower">
                <div class="view view-cascade gradient-card-header secondary-color">
                    <h3 class="h3-responsive card-header-title">Tiendas</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive text-nowrap">
                        <table class="table table-striped" id="DT_Stores">
                            <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Dueño</th>
                                <th scope="col">Nit</th>
                                <th scope="col" width="10%">Acciones</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/pages/admin_index_stores.js') }}"></script>
@endsection
