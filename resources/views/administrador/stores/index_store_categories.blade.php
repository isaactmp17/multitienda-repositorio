@extends('layouts.app_dashboard')

@section('head_styles')
    <!--<link href="{{asset('css/pages/admin_index_categories.css')}}" rel="stylesheet">-->
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-cascade narrower">
                <div class="view view-cascade gradient-card-header secondary-color">
                    <h3 class="h3-responsive card-header-title">Asociar categorías</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive text-nowrap">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Tienda</th>
                                    <th scope="col">Categorías</th>
                                    <th scope="col" width="10%">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                <tr>
                                    <td>{{$item->name}}</td>
                                    <td>
                                        @if(count($item->categories)>0)
                                            <?php $categories=array(); ?>
                                            @foreach($item->categories as $key=>$value)
                                                <?php array_push($categories,$value->name); ?>
                                            @endforeach
                                            {{implode(', ',$categories)}}
                                        @else
                                            No tiene ninguna categoría
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{asset('admin/stores/categories/'.$item->id)}}" class="btn btn-primary btn-sm p-2"><i class="fas fa-pencil-alt"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    {{ $items->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer_scripts')
    <!--<script type="text/javascript" src="{{ asset('js/pages/admin_index_categories.js') }}"></script>-->
@endsection
