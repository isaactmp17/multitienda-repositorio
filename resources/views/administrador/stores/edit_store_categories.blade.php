@extends('layouts.app_dashboard')

@section('head_styles')
    <link href="{{asset('css/pages/admin_edit_store_categories.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header secondary-color">
                    <h4 class="h4-responsive card-header-title">Tienda: {{$item->name}}</h4>
                </div>
                <form action="{{asset('admin/stores/categories/'.$item->id)}}" method="POST">
                    <div class="card-body">
                        @csrf
                        <strong class="d-block">Categorías</strong>
                        <select class="select2 form-control" name="categories[]" multiple="multiple">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}" {{in_array($category->id,$categories_selected)?'selected':''}}>{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                        <a href="{{asset('admin/stores/categories')}}" class="btn btn-secondary">Volver</a>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/pages/admin_edit_store_categories.js') }}"></script>
@endsection
