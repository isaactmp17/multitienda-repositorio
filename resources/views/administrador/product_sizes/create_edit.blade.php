@extends('layouts.app_dashboard')

@section('head_styles')
    <!--<link href="{{asset('css/pages/admin_create_edit_product_sizes.css')}}" rel="stylesheet">-->
@endsection

@section('content')
    <?php
        if(isset($item)){
            $item_options=json_decode($item->value);
        }
    ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <form action="{{(isset($item))?asset('admin/'.$uri.'/'.$item->id):asset('admin/'.$uri)}}"  enctype="multipart/form-data" method="POST">
                @csrf
                @isset($item)
                    @method('put')
                @endif
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="h4-responsive"><i class="fas fa-tags"></i> Crear Taxonomía</h4>
                    </div>
                    <div class="card-body">
                        <label class="d-block mt-3">Nombre <i class="fas fa-asterisk text-danger text-smaller"></i></label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', isset($item) ? $item->name : null) }}">
                        @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <label class="d-block mt-3">Atributos <i class="fas fa-asterisk text-danger text-smaller"></i></label>
                        <input type="text" name="options[]" class="form-control mb-3 @error('options.*') is-invalid @enderror" value="{{old('options')?old('options')[0]:isset($item)?$item_options[0]:null}}">
                        <div id="extra-options">
                            @if(old('options'))
                                @foreach(old('options') as $index=>$option)
                                    @if($index>0)
                                        <div class="input-group mb-3 extra-option">
                                            <input type="text" class="form-control" name="options[]" value="{{$option}}"><div class="input-group-append"><button class="btn btn-md btn-danger m-0 px-3 py-2 z-depth-0 waves-effect" onclick="deleteExtraOption()" type="button"><i class="fas fa-times"></i></button>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @else
                                @if(isset($item))
                                    @foreach($item_options as $index=>$option)
                                        @if($index>0)
                                            <div class="input-group mb-3 extra-option">
                                                <input type="text" class="form-control" name="options[]" value="{{$option}}"><div class="input-group-append"><button class="btn btn-md btn-danger m-0 px-3 py-2 z-depth-0 waves-effect" onclick="deleteExtraOption()" type="button"><i class="fas fa-times"></i></button>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            @endif
                        </div>
                        @error('options.*')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <button type="button" class="btn btn-warning btn-sm" id="add-extra-option">Agregar atributo</button>
                        <br><br>
                        <i class="fas fa-asterisk text-danger text-smaller"></i> Campos obligatorios
                    </div>
                    <div class="card-footer text-right">
                        <a href="{{asset('admin/'.$uri)}}" class="btn btn-secondary">Volver atrás</a>
                        <button class="btn btn-primary">{{isset($item)?'Guardar':'Crear'}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/pages/admin_create_edit_product_sizes.js') }}"></script>
@endsection
