@extends('layouts.app_dashboard')

@section('head_styles')
    <!--<link href="{{asset('css/pages/index_stores.css')}}" rel="stylesheet">-->
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <form action="{{(isset($item))?asset('admin/'.$uri.'/'.$item->id):asset('admin/'.$uri)}}"  enctype="multipart/form-data" method="POST" autocomplete="off">
                @csrf
                @isset($item)
                    @method('put')
                @endif
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="h4-responsive"><i class="fa fa-user"></i> Crear Usuario</h4>
                    </div>
                    <div class="card-body">
                        <label class="d-block mt-3">Nombre completo<i class="fas fa-asterisk text-danger text-smaller"></i></label>
                        <input type="text" name="fullname" class="form-control @error('fullname') is-invalid @enderror" value="{{ old('fullname', isset($item) ? $item->fullname : null) }}">
                        @error('fullname')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <label class="d-block mt-2">Correo <i class="fas fa-asterisk text-danger text-smaller"></i></label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email', isset($item) ? $item->email : null) }}">
                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        @if(!isset($item))
                            <label class="d-block mt-2">Contraseña <i class="fas fa-asterisk text-danger text-smaller"></i></label>
                            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}">
                            @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        @endif
                        <div class="form-group mt-2">
                            <label>Tipo de usuario</label>
                            <select name="role_id" class="form-control">
                                <?php $band=0; ?>
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}" {{isset($item)?($item->role_id==$role->id)?'selected':'':(old('role_id')==$role->id)?'selected':($role->id==3&&!isset($item))?'selected':''}}>{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('role_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group mt-2">
                            <label>Tipo de documento</label>
                            <select name="document_type" class="form-control">
                                <option value="">Seleccione...</option>
                                <option value="CC" {{isset($item)?($item->document_type=='CC')?'selected':'':(old('document_type')=='CC')?'selected':''}}>CC</option>
                                <option value="CE" {{isset($item)?($item->document_type=='CE')?'selected':'':(old('document_type')=='CE')?'selected':''}}>CE</option>
                                <option value="PASAPORTE" {{isset($item)?($item->document_type=='PASAPORTE')?'selected':'':(old('document_type')=='PASAPORTE')?'selected':''}}>Pasaporte</option>
                            </select>
                        </div>
                        @error('document_type')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <label class="d-block mt-3">Número de documento</label>
                        <input type="text" name="document_number" class="form-control @error('document_number') is-invalid @enderror" value="{{ old('document_number', isset($item) ? $item->document_number : null) }}">
                        @error('document_number')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group mt-2">
                            <label>Departamento</label>
                            <select name="state_id" id="state_id" class="mdb-select md-form mt-1" searchable="Buscar...">
                                <option value="" disabled selected>Seleccione...</option>
                                @foreach($states as $state)
                                    <option value="{{$state->id}}" {{isset($item)&&!is_null($item->city_id)?($item->city->state->id==$state->id)?'selected':'':''}}>{{$state->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('state_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group mt-2">
                            <label>Ciudad</label>
                            @if(isset($item)&&!is_null($item->city_id))
                                <?php
                                    $cities=\App\Helper::getCities($item->city->state->id);
                                ?>
                                <select name="city_id" id="city_id" class="form-control">
                                    <option value="">Seleccione...</option>
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}" {{isset($item)?($item->city_id==$city->id)?'selected':'':(old('city_id')==$city->id)?'selected':''}}>{{$city->name}}</option>
                                    @endforeach
                                </select>
                            @else
                                <select name="city_id" id="city_id" class="mdb-select md-form mt-1" searchable="Buscar...">
                                    <option value="" disabled selected>---</option>
                                </select>
                            @endif
                        </div>
                        @error('city_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group mt-2">
                            <label>Dirección</label>
                            <textarea class="form-control rounded-0 @error('address') is-invalid @enderror" name="address" rows="3">{{ isset($item)?$item->address:old('address') }}</textarea>
                        </div>
                        @error('address')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <br><br>
                        <i class="fas fa-asterisk text-danger text-smaller"></i> Campos obligatorios
                    </div>
                    <div class="card-footer text-right">
                        <a href="{{asset('admin/'.$uri)}}" class="btn btn-secondary">Volver atrás</a>
                        <button class="btn btn-primary">{{isset($item)?'Guardar':'Crear'}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/pages/admin_create_users.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.mdb-select').materialSelect();
        });
    </script>
@endsection
