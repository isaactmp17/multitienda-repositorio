@extends('layouts.app_dashboard')

@section('head_styles')
    <link href="{{asset('css/pages/index_stores.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="container">

    <div class="row">
        <div class="col-12 text-right mb-4">
            <a href="{{asset('admin/'.$uri.'/create')}}" class="btn btn-primary">Crear usuario</a>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-md-3">
            <div class="card">
                <div class="card-body text-center">
                    <div class="row flex-center">
                        <div class="col-md-6 text-center"><h1 class="h1-responsive text-primary"><i class="fas fa-users"></i></h1></div>
                        <div class="col-md-6 text-center"><h1 class="h1-responsive">{{count($users)}}</h1>
                        </div>
                    </div>

                </div>
                <div class="card-footer text-center bg-secondary font-weight-bold text-white">
                    Total de usuarios
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body text-center">
                    <div class="row flex-center">
                        <div class="col-md-6 text-center"><h1 class="h1-responsive text-primary"><i class="fas fa-user-tag"></i></h1></div>
                        <div class="col-md-6 text-center"><h1 class="h1-responsive">{{$users->where('role_id',3)->count()}}</h1>
                        </div>
                    </div>

                </div>
                <div class="card-footer text-center bg-secondary font-weight-bold text-white">
                    Clientes
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body text-center">
                    <div class="row flex-center">
                        <div class="col-md-6 text-center"><h1 class="h1-responsive text-primary"><i class="fas fa-user-check"></i></h1></div>
                        <div class="col-md-6 text-center"><h1 class="h1-responsive">{{$users->where('is_active',1)->count()}}</h1>
                        </div>
                    </div>

                </div>
                <div class="card-footer text-center bg-secondary font-weight-bold text-white">
                    Usuarios activos
                </div>
            </div>
        </div>
        <div class="col-md-3">

            <div class="card">
                <div class="card-body text-center">
                    <div class="row flex-center">
                        <div class="col-md-6 text-center"><h1 class="h1-responsive text-primary"><i class="fas fa-user-times"></i></h1></div>
                        <div class="col-md-6 text-center"><h1 class="h1-responsive">{{$users->where('is_active',0)->count()}}</h1>
                        </div>
                    </div>

                </div>
                <div class="card-footer text-center bg-secondary font-weight-bold text-white">
                    Usuarios suspendidos
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-cascade narrower">
                <div class="view view-cascade gradient-card-header secondary-color">
                    <h3 class="h3-responsive card-header-title">Usuarios</h3>
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="DT_Users">
                        <thead>
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Rol</th>
                                <th scope="col">Ciudad</th>
                                <th scope="col" width="10%">Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/pages/admin_index_users.js') }}"></script>
@endsection
