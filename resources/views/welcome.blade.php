<?php
    $control=round(count($categories)/2);
?>
@extends('layouts.app')
@section('seo')
    <meta name="robots" content="index, follow">
    <meta name="description" content="{{env('APP_DESCRIPTION')}}">
@endsection
@section('head_styles')
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <ul class="nav flex-column lighten-4 columna-categorias">
                    <li class="cabecera-columna">
                        Categorías
                    </li>
                    @for($i=0;$i<$control;$i++)
                    <li class="nav-item text-left">
                        <a class="nav-link" href="/categoria/{{$categories[$i]->slug}}">{{$categories[$i]->name}}</a>
                    </li>
                    @endfor
                </ul>
            </div>
            <div class="col-12 col-md-6">
                <img src="{{asset('statics/img/banners/banner-home-top-1.png')}}" class="img-fluid" alt="">
            </div>
            <div class="col-12 col-md-3">
                <ul class="nav flex-column lighten-4 columna-categorias">
                    <li class="cabecera-columna">
                        Categorías
                    </li>
                    @for($i=$control;$i<count($categories);$i++)
                        <li class="nav-item text-left">
                            <a class="nav-link" href="/categoria/{{$categories[$i]->slug}}">{{$categories[$i]->name}}</a>
                        </li>
                    @endfor
                </ul>
            </div>
        </div>
        @if(count($stores)>0)
            <?php $control=1; ?>
        <div class="row">
            <div class="col-12 py-4">
                <div id="brandsCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($stores as $index=>$store)
                            @if($control==1)
                                <div class="carousel-item {{($index==0)?'active':''}}">
                                    <div class="row flex-center">
                            @endif
                                <div class="col-4 col-md-2 text-center">
                                    <a href="{{asset('/tienda/'.$store->slug)}}">
                                        <img src="{{asset('statics/img/stores/'.$store->id.'.'.$store->photo.'?v='.strtotime($store->updated_at))}}" alt="{{$store->name}}" class="img-fluid" style="max-width:100%;">
                                    </a>
                                </div>
                            @if($control==5)
                                    </div>
                                </div>
                                <?php $control=1; ?>
                            @else
                                @if(count($stores)==($index+1))
                                    </div>
                                </div>
                                @endif
                                <?php $control++; ?>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="row flex-center justify-content-center">
            <div class="col-12 col-md-9">
                <div class="row">
                    <div class="col-12">
                        <h3 class="h3-responsive">Productos en Descuento</h3>
                    </div>
                </div>
                <div class="row">
                    @foreach($discount_products as $discount_product)
                    <a class="col-6 col-md-3"  href="{{asset('/tienda/'.$discount_product->store->slug.'/producto/'.$discount_product->slug)}}">
                        <img src="{{asset('statics/img/products/'.$discount_product->id.'/photo_1_270x370.'.$discount_product->photo_1.'?v='.strtotime($discount_product->updated_at))}}" class="img-fluid" alt="{{$discount_product->name}}">
                    </a>
                    @endforeach
                </div>
                <hr>
            </div>
            <div class="col-12 col-md-3 text-center">
                <img src="{{asset('statics/img/banners/banner-home-mid-1.jpg')}}" class="img-fluid" alt="">
            </div>
        </div>
        <div class="row flex-center justify-content-center mt-4">
            <div class="col-12 col-md-9">
                <div class="row">
                    <div class="col-12">
                        <h3 class="h3-responsive">Productos más vendidos</h3>
                    </div>
                </div>
                <div class="row">
                    @if(count($featured_products)>0)
                        @foreach($featured_products as $featured_product)
                        <div class="col-6 col-md-3 text-center">
                            <img src="{{asset('statics/img/products/'.$featured_product->id.'/photo_1_270x260.'.$featured_product->photo_1.'?v='.strtotime($featured_product->updated_at))}}" class="img-fluid" alt="{{$featured_product->name}}">
                            <span class="d-block mt-2">{{$featured_product->name}}</span>
                            <span class="d-block font-weight-bold text-primary">${{number_format($featured_product->price,0,',','.')}}</span>
                            <a class="btn btn-primary btn-sm" href="{{asset('/tienda/'.$discount_product->store->slug.'/producto/'.$featured_product->slug)}}">Ver Producto</a>
                        </div>
                        @endforeach
                    @else
                        @foreach($discount_products as $discount_product)
                            <div class="col-6 col-md-3 text-center">
                                <img src="{{asset('statics/img/products/'.$discount_product->id.'/photo_1_270x260.'.$discount_product->photo_1.'?v='.strtotime($discount_product->updated_at))}}" class="img-fluid" alt="{{$discount_product->name}}">
                                <span class="d-block mt-2">{{$discount_product->name}}</span>
                                <span class="d-block font-weight-bold text-primary">${{number_format($discount_product->price,0,',','.')}}</span>
                                <a class="btn btn-primary btn-sm" href="{{asset('/tienda/'.$discount_product->store->slug.'/producto/'.$discount_product->slug)}}">Ver Producto</a>
                            </div>
                        @endforeach
                    @endif
                </div>
                <hr>
            </div>
            <div class="col-12 col-md-3 text-center">
                <img src="{{asset('statics/img/banners/banner-home-mid-2.jpg')}}" class="img-fluid" alt="">
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12 col-md-6">
                <img src="{{asset('statics/img/banners/banner-home-bot-lg-1.jpg')}}" class="img-fluid" alt="">
            </div>
            <div class="col-12 col-md-6 mt-4 mt-md-0">
                <div class="row">
                    <div class="col-12">
                        <img src="{{asset('statics/img/banners/banner-home-bot-md-1.jpg')}}" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-6">
                        <img src="{{asset('statics/img/banners/banner-home-bot-sm-1.jpg')}}" class="img-fluid" alt="">
                    </div>
                    <div class="col-6">
                        <img src="{{asset('statics/img/banners/banner-home-bot-sm-2.jpg')}}" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

