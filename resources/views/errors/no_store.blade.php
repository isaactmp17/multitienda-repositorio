<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Ups! Error de tienda</title>
    <link rel="icon" href="{{ asset('statics/favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/mdb.min.css')}}" rel="stylesheet">
    <link href="{{asset('statics/css/style.css')}}" rel="stylesheet">
</head>
<body style="background-color: rgba(250,250,250,0)">
<div style="height: 100vh">
    <div class="flex-center flex-column">
        <h1 class="text-hide animated fadeIn mb-4" ><img src="{{asset('img/cono-error.png')}}" style="max-height: 200px" class="img-fluid" alt=""></h1>
        <h5 class="animated fadeIn mb-3 text-center">El administrador aún no te ha asignado una tienda</h5>
        <p class="animated fadeIn text-muted text-center">Por favor, ponte en contacto con el administrador del sitio para solucionar este error.</p>
        <button class="btn btn-secondary animated fadeIn" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form> Cerrar sesión
        </button>
    </div>
</div>
<script type="text/javascript">
    var plink="{{asset('/')}}";
</script>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/mdb.min.js')}}"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
