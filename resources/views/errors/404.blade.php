<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('page_title', env('APP_NAME'))</title>
    <link rel="icon" href="{{ asset('statics/favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/mdb.min.css')}}" rel="stylesheet">
    <link href="{{asset('statics/css/style.css')}}" rel="stylesheet">
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .code {
            border-right: 2px solid;
            font-size: 26px;
            padding: 0 15px 0 15px;
            text-align: center;
        }

        .message {
            font-size: 18px;
            text-align: center;
        }
    </style>
</head>
<body style="background-color: rgba(250,250,250,0)">
<div class="flex-center position-ref full-height flex-column">
    <h1 class="text-hide animated fadeIn mb-4 d-block" ><img src="{{asset('img/cono-error.png')}}" style="max-height: 200px" class="img-fluid" alt=""></h1>
    <div class="d-flex flex-row">
        <div class="code">
            404
        </div>
        <div class="message" style="padding: 10px;">
            Página no encontrada
        </div>
    </div>
    <a href="{{asset('/')}}" class="btn btn-primary mt-4">Volver al inicio</a>

</div>
<script type="text/javascript">
    var plink="{{asset('/')}}";
</script>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/mdb.min.js')}}"></script>
<script src="{{ asset('js/app.js') }}"></script>
@yield('footer_scripts')
</body>
</html>


