@extends('layouts.app_dashboard')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="row tienda-header py-4">
                <div class="col-12 my-3">
                    <h1 class="p-2">Administrador de pedidos</h1>
                </div>
            </div>
            @if(count($orders)>0)
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Nº Pedido</th>
                                <th>Tienda</th>
                                <th>Quién recibe</th>
                                <th>Total</th>
                                <th>Estado</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{$order->order_reference}}</td>
                                    <td>{{$order->store->name}}</td>
                                    <td>{{$order->fullname}}</td>
                                    <td>{{number_format($order->total_ammount,0,',','.')}} COP</td>
                                    <?php
                                    if($order->status==0){
                                        $status="Anulado";
                                    }elseif($order->status==1){
                                        $status="Pendiente de pago";
                                    }elseif($order->status==2){
                                        $status="Empacando su pedido";
                                    }elseif($order->status==3){
                                        $status="Pedido despachado";
                                    }else{
                                        $status="Pedido entregado";
                                    }
                                    ?>
                                    <td>{{$status}}</td>
                                    <td>
                                        <a class="btn btn-primary btn-sm py-1 px-2" href="{{asset('/manager/order/'.$order->order_reference)}}"><i class="fas fa-eye"></i></a>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        {{$orders->links()}}
                    </div>
                </div>
            @else
                <div class="row">
                    <h3 class="col-12 mt-4">No tienes pedidos.</h3>
                </div>
            @endif
        </div>

        <div class="col-md-4">
            <ul class="nav flex-column lighten-4 columna-categorias">
                <li class="cabecera-columna">
                    Usuario: {{Auth::user()->fullname}}
                </li>
                <li class="nav-item text-left">
                    <a class="nav-link" href="#">Mis pedidos</a>
                    <a class="nav-link" href="/manager/products">Mis productos</a>
                </li>
            </ul>
            <ul class="nav flex-column lighten-4 columna-categorias mt-3">
                <li class="cabecera-columna">
                    Estadísticas Básicas
                </li>
                <li class="nav-item text-left">
                    <a class="nav-link" href="#">Ventas de hoy: {{\App\Order::where('store_id',$store->id)->whereDate('created_at','=',\Carbon::today())->count()}} Venta(s)</a>
                    <a class="nav-link" href="#">Ventas del mes: {{\App\Order::where('store_id',$store->id)->whereDate('created_at','>=',date('Y-m-01'))->whereDate('created_at','<=',date('Y-m-t'))->count()}} Venta(s)</a>
                    <a class="nav-link" href="#">Ventas totales: {{\App\Order::where('store_id',$store->id)->count()}} Venta(s)</a>
                </li>
            </ul>
        </div>
    </div>
</div>

@endsection

@section('footer_scripts')
    <script>
        var ctxL = document.getElementById("lineChart1").getContext('2d');
        var myLineChart = new Chart(ctxL, {
            type: 'line',
            data: {
                labels: ["8 AM", "10 AM", "12 PM", "2 PM", "4 PM", "6 PM", "8 PM"],
                datasets: [{
                    fill: false,
                    borderColor: "#673ab7",
                    pointBackgroundColor: "#673ab7",
                    data: [885, 884, 887, 883, 888, 889, 888]
                }]
            },
            options: {
                responsive: false,
                legend: {
                    display: false
                },
                elements: {
                    line: {
                        tension: 0.0
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false,
                        },
                        ticks: {
                            padding: 15,
                            height: 30
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            drawBorder: false
                        },
                        ticks: {
                            maxTicksLimit: 5,
                            padding: 15,
                            min: 880,
                            max: 890
                        }
                    }]
                }
            }
        });
    </script>
@endsection
