@extends('layouts.app_dashboard')

@section('content')


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row tienda-header py-4">
                    <div class="col-12 my-3">
                        <h1 class="p-2">Pedido: {{$order->order_reference}}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 mt-4">
                <div class="card card-primary">
                    <div class="card-header font-weight-bold">
                        Información del pedido
                    </div>
                    <div class="card-body">
                        <strong>Nº Pedido</strong><br>
                        {{$order->order_reference}}<br><br>
                        <strong>Fecha del pedido</strong><br>
                        {{date('d-m-Y h:i A',strtotime($order->created_at))}}<br><br>
                        <strong>Nombre de quién recibe</strong><br>
                        {{$order->fullname}}<br><br>
                        <strong>Dirección de quién recibe</strong><br>
                        {{$order->address}}<br><br>
                        <strong>Ciudad, Departamento.</strong><br>
                        {{$order->city->name.', '.$order->city->state->name}}<br><br>
                        <strong>Teléfono de contacto</strong><br>
                        {{$order->phone}}<br><br>
                        <strong>Referencia de Payu</strong><br>
                        {{!is_null($order->reference_pol)?$order->reference_pol:'-'}}<br><br>
                        <strong>Respuesta de Payu</strong><br>
                        {{!is_null($order->payu_response)?$order->payu_response:'-'}}<br><br>
                        <strong>Estado del pedido</strong><br>
                        <?php
                            $status_name=['Anulado','Pendiente de pago','Empacando su pedido','Pedido despachado','Pedido entregado'];
                        ?>
                        <form action="/manager/order/{{$order->order_reference}}/update_status" method="POST">
                            @csrf
                            <select class="form-control" name="status">
                                @for($i=0;$i<=4;$i++)
                                    <option value="{{$i}}" {{$i==$order->status?'selected':''}}>{{$status_name[$i]}}</option>
                                @endfor
                            </select>
                            <button class="btn btn-primary m-0 btn-block">Actualizar</button>
                        </form>
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-secondary btn-block" href="/home">Volver atrás</a>
                    </div>
                </div>
            </div>
            <div class="col-md-8 mt-4">
                <div class="card card-primary">
                    <div class="card-header font-weight-bold">
                        Detalles del pedido
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th class="text-center">Cantidad</th>
                                    <th class="text-center">Precio unitario</th>
                                    <th class="text-right">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $total_ammount=0;
                                ?>
                                @foreach($order->orderProducts as $product)
                                    <?php
                                        $total_ammount+=$product->quanty*$product->price;
                                    ?>
                                    <tr>
                                        <td>{{$product->name}} {{is_null($product->taxonomy)?'':'- '.$product->taxonomy}}</td>
                                        <td align="center">{{$product->quanty}}</td>
                                        <td align="center">{{number_format($product->price,0,',','.')}} COP</td>
                                        <td align="right">{{number_format($product->quanty*$product->price,0,',','.')}} COP</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3" align="right" class="font-weight-bold"> Total a pagar:</td>
                                    <td align="right">{{number_format($total_ammount,0,',','.')}} COP</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card card-primary mt-4">
                    <div class="card-header font-weight-bold">
                        Datos del cliente
                    </div>
                    <div class="card-body">
                        Estos son los datos del cliente que realizó el pedido, aunque puede coincidir con la información de quién recibe, se puede dar el caso en que no.<br>
                        <table class="table table-striped mt-2">
                            <tr>
                                <th>Nombre:</th>
                                <td>{{$order->user->fullname}}</td>
                            </tr>
                            <tr>
                                <th>Correo:</th>
                                <td>{{$order->user->email}}</td>
                            </tr>
                            <tr>
                                <th>Dirección:</th>
                                <td>{{$order->user->address}}</td>
                            </tr>
                            <tr>
                                <th>Ciudad, Departamento:</th>
                                <td>
                                    @if(!is_null($order->user->city_id))
                                        {{$order->user->city->name}}, {{$order->user->city->state->name}}
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Usuario desde:</th>
                                <td>{{date('d-m-Y h:i A',strtotime($order->user->created_at))}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
@endsection
