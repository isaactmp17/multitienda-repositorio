@extends('layouts.app_dashboard')

@section('head_styles')
    <!--<link href="{{asset('css/pages/manager_create_edit_products.css')}}" rel="stylesheet">-->
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <form action="{{(isset($item))?asset('manager/'.$uri.'/'.$item->id):asset('manager/'.$uri)}}"  enctype="multipart/form-data" method="POST">
                @csrf
                @isset($item)
                    @method('put')
                @endif
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="h4-responsive"><i class="fas fa-cubes"></i> {{isset($item)?'Modificar':'Crear'}} Producto</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="h5-responsive">Detalles del producto</h5>
                                <hr>
                                <label class="d-block mt-3">Nombre <i class="fas fa-asterisk text-danger text-smaller"></i></label>
                                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', isset($item) ? $item->name : null) }}" required>
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group mt-2">
                                    <label>Descripción corta <i class="fas fa-asterisk text-danger text-smaller"></i></label>
                                    <textarea class="form-control rounded-0 @error('description') is-invalid @enderror" name="description" rows="3" required>{{ old('description', isset($item) ? $item->description : null) }}</textarea>
                                </div>
                                @error('description')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <label class="d-block mt-3">Precio actual <i class="fas fa-asterisk text-danger text-smaller"></i></label>
                                <input type="number" name="price" class="form-control @error('price') is-invalid @enderror validate" value="{{ old('price', isset($item) ? round($item->price) : null) }}" min="0" required>
                                @error('price')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <label class="d-block mt-3">Precio mayorista <i class="fas fa-asterisk text-danger text-smaller"></i></label>
                                <input type="number" name="wholesaler_price" class="form-control @error('wholesaler_price') is-invalid @enderror validate" value="{{ old('wholesaler_price', isset($item) ? round($item->wholesaler_price) : null) }}" min="0" required>
                                @error('wholesaler_price')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <label class="d-block mt-3">Precio anterior</label>
                                <input type="number" name="old_price" class="form-control @error('old_price') is-invalid @enderror validate" value="{{ old('old_price', isset($item) ? !is_null($item->old_price)?round($item->old_price):null : null) }}" min="0">
                                @error('old_price')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <?php
                                $type='s';
                                if(old('type')){
                                    $type=old('type');
                                }else{
                                    if(isset($item)){
                                        $type=$item->type;
                                    }
                                }
                                ?>
                                @if(!isset($item))
                                    <div class="form-group mt-2">
                                        <label>Tipo de producto</label>
                                        <select name="type" id="type" class="form-control @error('typealkost') is-invalid @enderror">
                                            <option value="s" {{ $type=='s'?'selected':'' }}>Simple</option>
                                            <option value="a" {{ $type=='a'?'selected':'' }}>Avanzado</option>
                                        </select>
                                    </div>
                                    @error('type')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <div class="form-group mt-2 hide" id="taxonomy_row">
                                        <label>Taxonomía</label>
                                        <select name="product_size_id" id="product_size_id" class="form-control">
                                            @foreach($product_sizes as $product_size)
                                                <option value="{{$product_size->id}}" {{ old('product_size_id',isset($item)?$item->product_size_id:null)==$product_size->id?'selected':'' }}>{{$product_size->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @error('product_size_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                @else
                                    @if($item->type=='a')
                                        <label class="mt-2">Taxonomía: {{$item->productSize->name}}</label>.
                                    @endif
                                @endif
                                <?php
                                $stock=0;
                                if(old('stock')){
                                    $stock=old('stock');
                                }else{
                                    if(isset($item)){
                                        if($item->type=='s'){
                                            $stock=$item->stock;
                                        }
                                    }
                                }
                                ?>
                                @if(isset($item))
                                    @if($item->type=='s')
                                        <div>
                                            <label class="d-block mt-3 font-weight-bold">Stock <i class="fas fa-asterisk text-danger text-smaller"></i></label>
                                            <input type="number" name="stock" class="form-control @error('stock') is-invalid @enderror validate" value="{{ $stock }}" min="0" required>
                                            @error('stock')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @else
                                        @foreach(json_decode($item->stock) as $taxonomy)
                                            <label class="d-block mt-3 font-weight-bold">{{$taxonomy[0]}} stock</label>
                                            <input type="number" name="taxonomies[]" class="form-control validate" value="{{$taxonomy[1]}}" min="0" required>
                                        @endforeach
                                    @endif
                                @else
                                    <div class="{{isset($item)?$item->type=='s'?'':'hide':''}}" id="stock_s_row">
                                        <label class="d-block mt-3 font-weight-bold">Stock <i class="fas fa-asterisk text-danger text-smaller"></i></label>
                                        <input type="number" name="stock" class="form-control @error('stock') is-invalid @enderror validate" value="{{ $stock }}" min="0">
                                        @error('stock')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                @endif
                                <div id="taxonomies_html"></div>

                            </div>
                            <div class="col-md-6">
                                <h5 class="h5-responsive mt-4 mt-md-0">Fotos del producto</h5>
                                <hr>
                                <label class="d-block mt-3">Foto 1 @if(!isset($item)) <i class="fas fa-asterisk text-danger text-smaller"></i> @endif</label>
                                @if(isset($item))
                                    @if(!is_null($item->photo_1))
                                        <img src="{{asset('statics/img/products/'.$item->id.'/photo_1_270x260.'.$item->photo_1.'?v='.strtotime($item->updated_at))}}" class="mb-1" height="70px">
                                    @endif
                                @endif
                                <div class="custom-file">
                                    <input type="file" name="photo_1" class="custom-file-input @error('photo_1') is-invalid @enderror" id="customFileLang1" lang="es" value="{{ old('photo_1') }}">
                                    <label class="custom-file-label" for="customFileLang1">Seleccionar Archivo</label>
                                </div>
                                @error('photo_1')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <label class="d-block mt-3">Foto 2 @if(!isset($item)) @endif</label>

                                @if(isset($item))
                                    @if(!is_null($item->photo_2))
                                        <img src="{{asset('statics/img/products/'.$item->id.'/photo_2_270x260.'.$item->photo_2.'?v='.strtotime($item->updated_at))}}" class="mb-1" height="70px">
                                    @endif
                                @endif
                                <div class="custom-file">
                                    <input type="file" name="photo_2" class="custom-file-input @error('photo_2') is-invalid @enderror" id="customFileLang2" lang="es" value="{{ old('photo_2') }}">
                                    <label class="custom-file-label" for="customFileLang2">Seleccionar Archivo</label>
                                </div>
                                @error('photo_2')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <label class="d-block mt-3">Foto 3 @if(!isset($item)) @endif</label>

                                @if(isset($item))
                                    @if(!is_null($item->photo_3))
                                        <img src="{{asset('statics/img/products/'.$item->id.'/photo_3_270x260.'.$item->photo_3.'?v='.strtotime($item->updated_at))}}" class="mb-1" height="70px">
                                    @endif
                                @endif
                                <div class="custom-file">
                                    <input type="file" name="photo_3" class="custom-file-input @error('photo_3') is-invalid @enderror" id="customFileLang3" lang="es" value="{{ old('photo_3') }}">
                                    <label class="custom-file-label" for="customFileLang3">Seleccionar Archivo</label>
                                </div>
                                @error('photo_3')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <br><br>
                        <i class="fas fa-asterisk text-danger text-smaller"></i> Campos obligatorios
                    </div>
                    <div class="card-footer text-right">
                        <a href="{{asset('manager/'.$uri)}}" class="btn btn-secondary">Volver atrás</a>
                        <button class="btn btn-primary">{{isset($item)?'Guardar':'Crear'}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/pages/manager_create_edit_products.js') }}"></script>
    @if($errors->any())
        @if($type=='a')
            <script>
                swal({
                    title:'',
                    text:'Verifica el stock, ha sido reiniciado a 0.',
                    icon:'warning',
                    closeOnEsc: false,
                    closeOnClickOutside: false
                })
            </script>
        @endif
    @endif
@endsection
