@foreach(json_decode($item->value) as $taxonomy)
    <label class="d-block mt-3 font-weight-bold">{{$taxonomy}} stock</label>
    <input type="number" name="taxonomies[]" class="form-control validate" value="0" min="0" required>
@endforeach

<script>
    $(".validate").keydown(function(event) {
        if(event.shiftKey)
        {
            event.preventDefault();
        }

        if (event.keyCode == 46 || event.keyCode == 8)    {
        }
        else {
            if (event.keyCode < 95) {
                if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();
                }
            }
            else {
                if (event.keyCode < 96 || event.keyCode > 105) {
                    event.preventDefault();
                }
            }
        }
    });
</script>
