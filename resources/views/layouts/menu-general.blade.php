<nav class="navbar navbar-expand-md navbar-dark menu-general-bg shadow-sm sticky-top scrolling-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{asset('statics/img/logo.png')}}" height="80px" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto mr-auto">
                <li>
                    <form action="/buscar" method="GET">
                        <div class="input-group buscador">
                            <input type="text" class="form-control" name="q" placeholder="Buscar..." value="{{isset($query)?$query:''}}">
                            <div class="input-group-append">
                                <button class="btn btn-md btn-primary m-0 px-3 py-2 z-depth-0 waves-effect" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto flex-center">
                @if(isset($shop_cart))
                    <?php
                        $cant=0;
                        foreach ($shop_cart->items as $item){
                            $cant+=$item['quanty'];
                        }
                    ?>
                    <li class="nav-item">
                        <a class="nav-link waves-effect waves-light" href="/tienda/{{$shop_cart->slug}}/cart">
                            <span class="badge badge-pill badge-danger">{{$cant}}</span>
                            <i class="fas fa-shopping-cart"></i>
                        </a>
                    </li>
                @endif
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('login')}}">
                            <span class="user-icon"><i class="fas fa-user"></i></span>
                            Usuarios Registrados

                        </a>
                    </li>
                @else
                    <li class="nav-item avatar dropdown">
                        <a class="nav-link dropdown-toggle" id="dropDownUser" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <span class="user-icon"><i class="fas fa-user"></i></span>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            {{Auth::user()->fullname}}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-dark"
                             aria-labelledby="dropDownUser">
                            <a class="dropdown-item" href="/home">
                                Dashboard
                            </a>
                            <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                Cerrar Sesión
                            </a>
                        </div>
                    </li>
                @endguest
               <!-- @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
                   -->
            </ul>
        </div>
    </div>
</nav>
