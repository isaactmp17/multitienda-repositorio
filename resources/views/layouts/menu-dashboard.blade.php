<nav class="mb-1 navbar navbar-dark secondary-color lighten-1 navbar-toggler-right sticky-top d-block">
    <div class="row flex-center">
        <div class="col-4">
            <button href="#" data-activates="slide-out" class="btn btn-primary px-3 py-2 button-collapse" id="sideBarButton"><i class="fas fa-bars"></i></button>
        </div>
        <div class="col-4 text-center">
            <img src="{{asset('statics/img/logo.png')}}" height="50px">
        </div>
        <div class="col-4 text-right">
            <ul class="navbar-nav float-right nav-flex-icons">
                <li class="nav-item avatar dropdown">
                    <a class="nav-link dropdown-toggle" id="dropDownUser" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <img src="{{asset('img/avatar.png')}}" class="rounded-circle z-depth-0"
                             alt="avatar image">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-dark"
                         aria-labelledby="dropDownUser">
                        <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            Cerrar Sesión
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>


<div id="slide-out" class="side-nav fixed special-color-dark">
    <ul class="custom-scrollbar">
        <li>
            <div class="logo-wrapper waves-light">
                <a href="#"><img src="{{asset('statics/img/logo.png')}}"
                                 class="img-fluid flex-center" style="max-height: 85px"></a>
            </div>
        </li>
        <li class="mt-0">
            <ul class="collapsible collapsible-accordion">
                <li><a href="{{asset('home')}}" class="waves-effect"><i class="fas fa-home"></i>Dashboard</a></li>
                @if(Auth::user()->role_id==1)
                    <li><a href="{{asset('admin/categories')}}" class="waves-effect"><i class="fas fa-grip-vertical"></i>Categorías</a></li>
                    <li><a class="collapsible-header waves-effect arrow-r"><i class="fas fa-store"></i>
                            Tiendas<i class="fas fa-angle-down rotate-icon"></i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="{{asset('admin/stores')}}" class="waves-effect">Administrar</a>
                                </li>
                                <li><a href="{{asset('admin/stores/categories')}}" class="waves-effect">Asociar categorías</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="{{asset('admin/product_sizes')}}" class="waves-effect"><i class="fas fa-tags"></i>Taxonomía de Productos</a></li>
                    <li><a href="{{asset('admin/users')}}" class="waves-effect"><i class="fas fa-users"></i>Usuarios</a></li>

                @endif
                @if(Auth::user()->role_id==2)
                    <li><a href="{{asset('manager/products')}}" class="waves-effect"><i class="fas fa-cubes"></i>Productos</a></li>

                @endif
            </ul>
        </li>
    </ul>
    <div class="sidenav-bg"></div>
</div>
