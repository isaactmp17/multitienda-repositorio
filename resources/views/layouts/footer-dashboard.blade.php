<footer class="d-block bg-footer mt-4">
    <div class="container text-white">
        <div class="row">
            <div class="col-md-8 text-center offset-2 py-4">
                {!! env('APP_COPYRIGHT',"Todos los derechos reservados &copy;") !!}
            </div>
        </div>
    </div>
</footer>
