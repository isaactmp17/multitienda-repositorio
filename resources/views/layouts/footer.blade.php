<footer class="d-block bg-footer mt-4">
    <div class="container text-white">
        <div class="row">
            <div class="col-md-4 text-center py-4">
                <a href=""><i class="fas fa-store"></i> Shipping</a>
            </div>
            <div class="col-md-4 text-center py-4">
                <a href=""><i class="far fa-credit-card"></i> Payment</a>
            </div>
            <div class="col-md-4 text-center py-4">
                <a href=""><i class="fas fa-undo"></i> Return Policy</a>
            </div>
        </div>
    </div>
</footer>
