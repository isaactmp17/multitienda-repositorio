$(function () {
    $('#state_id').on('change',function(){
        $.ajax({
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            data:'',
            url:'/get/cities/'+$(this).val(),
            type:'GET',
            beforeSend:function(){
                swal({
                    title: 'Espere un momento por favor',
                    text:'Estamos cargando las ciudades',
                    icon:'info',
                    buttons: false,
                    closeOnEsc: false,
                    closeOnClickOutside: false
                })
            },
            success:function (response) {
                $('#city_id').html('');
                $.each(response,function(index,value){
                    $('#city_id').append('<option value="'+value.id+'">'+value.name+'</option>');
                });
                swal({
                    title: 'Excelente',
                    text:'Ciudades cargadas correctamente.',
                    icon:'success',
                    closeOnEsc: false,
                    closeOnClickOutside: false
                })
            }
        })
    });
});
