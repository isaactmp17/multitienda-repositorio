$(function () {
    deleteExtraOption = function(){
        var length_options=$('.extra-option').length;
        if(length_options>0){
            $('.extra-option')[$('.extra-option').length-1].remove();
        }
    }
    $('#add-extra-option').on('click',function (e) {
        e.preventDefault();
        var length_options=$('.extra-option').length;
        if(length_options<=9) {
            var html_append = '<div class="input-group mb-3 extra-option"><input type="text" class="form-control" name="options[]"><div class="input-group-append"><button class="btn btn-md btn-danger m-0 px-3 py-2 z-depth-0 waves-effect" onclick="deleteExtraOption()" type="button"><i class="fas fa-times"></i></button></div></div>';
            $('#extra-options').append(html_append);
        }else{
            swal({
                'title':'Ups!',
                'text':'Ha alcanzado el máximo de atributos',
                'icon':'warning'
            })
        }

    })
});
