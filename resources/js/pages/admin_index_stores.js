$(function () {
    var table = $('#DT_Stores').DataTable({
        language: {
            "url": "/lang/dt_spanish.json"
        },
        responsive:true,
        processing: true,
        serverSide: true,
        ajax: "/admin/stores",
        columns: [
            {data: 'photo', name: 'photo'},
            {data: 'name', name: 'name'},
            {data: 'owner', name: 'owner'},
            {data: 'nit', name: 'nit'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    $('body').on('click','.change-status',function (e) {
        $.ajax({
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            data:{'id':$(this).data('target')},
            type:'POST',
            url:'/admin/stores/update_status',
            success:function (res) {
                if(res.band==1){
                    swal({
                        title: 'Excelente!',
                        text: 'El estado de la tienda ha sido actualizado correctamente.',
                        icon: 'success',
                        closeOnEsc: false,
                        closeOnClickOutside: false
                    })
                }else{
                    swal({
                        title: 'Ups!',
                        text: 'Hubo un problema al actualizar el estado de la tienda.',
                        icon: 'error',
                        closeOnEsc: false,
                        closeOnClickOutside: false
                    }).then((value) => {
                        if(value)
                            window.location=window.location;
                    });
                }
            }
        })
    })
});
