$(function () {
    var table = $('#DT_Users').DataTable({
        language: {
            "url": "/lang/dt_spanish.json"
        },
        responsive:true,
        processing: true,
        serverSide: true,
        ajax: "/admin/users",
        columns: [
            {data: 'fullname', name: 'fullname'},
            {data: 'email', name: 'email'},
            {data: 'role', name: 'role'},
            {data: 'city', name: 'city'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $('body').on('click','.change-status',function (e) {
        $.ajax({
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            data:{'id':$(this).data('target')},
            type:'POST',
            url:'/admin/users/update_status',
            success:function (res) {
                if(res.band==1){
                    swal({
                        title: 'Excelente!',
                        text: 'El estado del usuario ha sido actualizado correctamente.',
                        icon: 'success',
                        closeOnEsc: false,
                        closeOnClickOutside: false
                    }).then((value) => {
                        if(value)
                            window.location=window.location;
                    });
                }else{
                    swal({
                        title: 'Ups!',
                        text: 'Hubo un problema al actualizar el estado del usuario.',
                        icon: 'error',
                        closeOnEsc: false,
                        closeOnClickOutside: false
                    }).then((value) => {
                        if(value)
                            window.location=window.location;
                    });
                }
            }
        })
    })
});
