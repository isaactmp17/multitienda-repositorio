$(function () {
    var table = $('#DT_MProducts').DataTable({
        language: {
            "url": "/lang/dt_spanish.json"
        },
        responsive:true,
        processing: true,
        serverSide: true,
        ajax: "/manager/products",
        columns: [
            {data: 'photo', name: 'photo'},
            {data: 'name', name: 'name'},
            {data: 'stock', name: 'stock'},
            {data: 'is_featured', name: 'is_featured'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    $('body').on('click','.change-status',function (e) {
        $.ajax({
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            data:{'id':$(this).data('target')},
            type:'POST',
            url:'/manager/products/update_status',
            success:function (res) {
                if(res.band==1){
                    swal({
                        title: 'Excelente!',
                        text: 'El estado del producto ha sido actualizado correctamente.',
                        icon: 'success',
                        closeOnEsc: false,
                        closeOnClickOutside: false
                    })
                }else{
                    swal({
                        title: 'Ups!',
                        text: 'Hubo un problema al actualizar el estado del producto.',
                        icon: 'error',
                        closeOnEsc: false,
                        closeOnClickOutside: false
                    }).then((value) => {
                        if(value)
                            window.location=window.location;
                    });
                }
            }
        })
    })
    $('body').on('click','.change-featured',function (e) {
        $.ajax({
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            data:{'id':$(this).data('target')},
            type:'POST',
            url:'/manager/products/update_featured',
            success:function (res) {
                if(res.band==1){
                    swal({
                        title: 'Excelente!',
                        text: 'El producto ha sido actualizado.',
                        icon: 'success',
                        closeOnEsc: false,
                        closeOnClickOutside: false
                    })
                    return true;
                }else if(res.band==2){
                    e.preventDefault();
                    swal({
                        title: '',
                        text: 'Solo pueden haber 4 productos destacados.',
                        icon: 'warning',
                        closeOnEsc: false,
                        closeOnClickOutside: false
                    }).then((value) => {
                        if(value)
                            table.ajax.reload();
                    });

                }else{
                    swal({
                        title: 'Ups!',
                        text: 'Hubo un problema al actualizar el estado del producto.',
                        icon: 'error',
                        closeOnEsc: false,
                        closeOnClickOutside: false
                    }).then((value) => {
                        if(value)
                            window.location=window.location;
                    });
                }
            }
        })
    })
});
