
$(function () {
    $(".validate").keydown(function(event) {
        if(event.shiftKey)
        {
            event.preventDefault();
        }

        if (event.keyCode == 46 || event.keyCode == 8)    {
        }
        else {
            if (event.keyCode < 95) {
                if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();
                }
            }
            else {
                if (event.keyCode < 96 || event.keyCode > 105) {
                    event.preventDefault();
                }
            }
        }
    });
    function loadProductSizes() {
        $.ajax({
            url:'/manager/products/get_product_sizes',
            data:{'id':$('#product_size_id').val()},
            type:'post',
            success: function (response) {
                $('#taxonomies_html').html(response);
            },
            statusCode: {
                404: function() {
                    alert('Web not found');
                }
            },
            error:function(x,xs,xt){
                console.log(x);
                //window.open(JSON.stringify(x));
            }
        });
    }
    function showProductSizes() {
        if ($('#type').val() == 's') {
            if (!$('#taxonomy_row').hasClass('hide'))
                $('#taxonomy_row').addClass('hide');
            if ($('#stock_s_row').hasClass('hide'))
                $('#stock_s_row').removeClass('hide');
            $('#taxonomies_html').html("");
        } else {
            loadProductSizes();
            if ($('#taxonomy_row').hasClass('hide'))
                $('#taxonomy_row').removeClass('hide');
            if (!$('#stock_s_row').hasClass('hide'))
                $('#stock_s_row').addClass('hide');
        }

    }
    showProductSizes();

    $('#type').on('change',function(){
        showProductSizes();
    });
    $('#product_size_id').on('change',function(){
        showProductSizes();
    });
});
