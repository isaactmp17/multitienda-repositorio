$(function () {
    var table = $('#DT_Taxonomies').DataTable({
        language: {
            "url": "/lang/dt_spanish.json"
        },
        responsive:true,
        processing: true,
        serverSide: true,
        ajax: "/admin/product_sizes",
        columns: [
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
});
