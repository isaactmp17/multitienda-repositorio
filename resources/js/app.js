/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.swal = require('sweetalert');
//require('./bootstrap');

//window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/**const app = new Vue({
    el: '#app',
});**/
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(function (){
    $('#add-quanty').on('click',function (e){
        e.preventDefault();
        if((parseInt($('#quanty').val())+1)<=$('#quanty').data('max')){
            $('#quanty').val(parseInt($('#quanty').val())+1);
        }else{
            swal({
                title: 'Ups!',
                text: 'Alcanzó el máximo disponible de productos.',
                icon: 'warning',
                closeOnEsc: false,
                closeOnClickOutside: false
            });
        }
    });
    $('#remove-quanty').on('click',function (e){
        e.preventDefault();
        if(parseInt($('#quanty').val())>0){
            $('#quanty').val(parseInt($('#quanty').val())-1);
        }else{
            swal({
                title: 'Ups!',
                text: 'Ya no puede quitar más cantidad.',
                icon: 'warning',
                closeOnEsc: false,
                closeOnClickOutside: false
            });
        }
    });
    $('.add-quanty').on('click',function (e){
        e.preventDefault();
        var target=$(this).data('target');
        if((parseInt($('#quanty'+target).val())+1)<=$('#quanty'+target).data('max')){
            $('#quanty'+target).val(parseInt($('#quanty'+target).val())+1);
        }else{
            swal({
                title: 'Ups!',
                text: 'Alcanzó el máximo disponible de productos.',
                icon: 'warning',
                closeOnEsc: false,
                closeOnClickOutside: false
            });
        }
    });
    $('.remove-quanty').on('click',function (e){
        e.preventDefault();
        var target=$(this).data('target');
        if(parseInt($('#quanty'+target).val())>1){
            $('#quanty'+target).val(parseInt($('#quanty'+target).val())-1);
        }else{
            swal({
                title: 'Ups!',
                text: 'Ya no puede quitar más cantidad.',
                icon: 'warning',
                closeOnEsc: false,
                closeOnClickOutside: amazofalse
            });
        }
    });
    $('.remove-product').on('click',function (e){
        e.preventDefault();
        var target=$(this).data('target'),taxonomy=$(this).data('taxonomy');
        $.ajax({
            data:{'target':target,'taxonomy':taxonomy},
            url:'cart/delete_item',
            type:'POST',
            success:function (response){
                if(response.band==1){
                    swal({
                        title: 'Excelente!',
                        text: 'Producto eliminado correctamente de la cesta.',
                        icon: 'warning',
                        closeOnEsc: false,
                        closeOnClickOutside: false
                    }).then((value) => {
                        if(value)
                            window.location=window.location;
                    });
                }
            }
        })
    });

})
$('.carousel').carousel({
    touch: true // default
});
