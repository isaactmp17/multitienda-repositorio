const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

// JS Scripts
mix.scripts([
    'resources/js/plugins/datatables.min.js',
    'resources/js/pages/admin_index_stores.js',
], 'public/js/pages/admin_index_stores.js');
mix.scripts([
    'resources/js/plugins/datatables.min.js',
    'resources/js/pages/admin_index_users.js',
], 'public/js/pages/admin_index_users.js');
mix.scripts([
    'resources/js/pages/admin_create_users.js',
], 'public/js/pages/admin_create_users.js');
mix.scripts([
    'resources/js/plugins/datatables.min.js',
    'resources/js/pages/admin_index_categories.js',
], 'public/js/pages/admin_index_categories.js');
mix.scripts([
    'resources/js/plugins/select2.min.js',
    'resources/js/pages/admin_edit_store_categories.js',
], 'public/js/pages/admin_edit_store_categories.js');
mix.scripts([
    'resources/js/pages/admin_create_edit_product_sizes.js',
], 'public/js/pages/admin_create_edit_product_sizes.js');
mix.scripts([
    'resources/js/plugins/datatables.min.js',
    'resources/js/pages/admin_index_product_sizes.js',
], 'public/js/pages/admin_index_product_sizes.js');
mix.scripts([
    'resources/js/pages/manager_create_edit_products.js',
], 'public/js/pages/manager_create_edit_products.js');

mix.scripts([
    'resources/js/plugins/datatables.min.js',
    'resources/js/pages/manager_index_products.js',
], 'public/js/pages/manager_index_products.js');


// CSS Styles
mix.styles([
    'resources/css/plugins/datatables.min.css',
], 'public/css/pages/admin_index_stores.css');
mix.styles([
    'resources/css/plugins/datatables.min.css',
], 'public/css/pages/admin_index_users.css');
mix.styles([
    'resources/css/plugins/datatables.min.css',
], 'public/css/pages/admin_index_categories.css');
mix.styles([
    'resources/css/plugins/select2.min.css',
], 'public/css/pages/admin_edit_store_categories.css');

mix.styles([
    'resources/css/plugins/datatables.min.css',
], 'public/css/pages/admin_index_product_sizes.css');
mix.styles([
    'resources/css/plugins/datatables.min.css',
], 'public/css/pages/manager_index_products.css');
